#include "SmsBuffer.h"

#include <stdlib.h>

uint8_t initSMSBuffer(SmsBuffer_t* rBuffer, uint16_t size)
{
    rBuffer->buffer = (SMS_Instance*)malloc(size * sizeof(SMS_Instance));
    rBuffer->size = size;

    return rBuffer->buffer != NULL;
}
void deinitSMSBuffer(SmsBuffer_t* rBuffer)
{
    free(rBuffer->buffer);
    rBuffer->size = 0;
}

void smsBuffer_put(SmsBuffer_t* rBuffer, SMS_Instance data)
{
	  rBuffer->buffer[rBuffer->idxIn++] = data;
	  if(rBuffer->idxIn >= rBuffer->size) 
	  {
	    	rBuffer->idxIn = 0;
	  }
}

SMS_Instance smsBuffer_pop(SmsBuffer_t* rBuffer)
{
	SMS_Instance retval = rBuffer->buffer[rBuffer->idxOut++];
	if(rBuffer->idxOut >= rBuffer->size)
	{
	  	rBuffer->idxOut = 0;	
	}
	return retval;
}

uint16_t smsBuffer_getCount(SmsBuffer_t* rBuffer)
{
	uint16_t retval = 0;
	if(rBuffer->idxIn < rBuffer->idxOut)
	{
	  	retval = rBuffer->size + rBuffer->idxIn - rBuffer->idxOut;
	}
	else
	{
	  	retval = rBuffer->idxIn - rBuffer->idxOut;
	}
	return retval;
}

