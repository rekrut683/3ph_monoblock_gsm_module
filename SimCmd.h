#ifndef	_SIM_CMD_H
#define _SIM_CMD_H

//#include "SIM800C.h"
#include <stdint.h>

#define MTS
//#define BEELINE
//#define MEGAFON

#define COMMANDS_INIT_SIZE					5
#define COMMANDS_CLIENT_SIZE				2
#define COMMANDS_SMS_SIZE					4
#define COMMANDS_PUSH_SIZE					3

#define RESPONSE_STRING_SIZE				20
#define RESPONSE_ARRAY_SIZE					2

typedef enum SIM800_ERROR_ENUM
{
    E_NONE          = 0,

    E_SIM_LINK_UART = 1,
    E_SIM_SIMCARD   = 2,
    E_SIM_QUALITY   = 3,
    E_SIM_VOLTAGE   = 4,

    E_GATT          = 5,
    E_RESET_CON     = 6,
    E_IP_STATUS     = 7,
    E_APN_STATUS    = 8,
    E_CONTEXT_GPRS  = 9,
    E_IP_ADDR       = 10
} SIM800_ERROR;

typedef struct COMMAND_STRUCT_TYPE
{
    uint8_t					cmd[40];
    uint8_t					resp[RESPONSE_ARRAY_SIZE][RESPONSE_STRING_SIZE];
    uint8_t					badResp[RESPONSE_ARRAY_SIZE][RESPONSE_STRING_SIZE];
    SIM800_ERROR			error;
    uint8_t					needAddRequest;
    uint32_t				timeoutResponse;
    uint8_t					needToRetry;
} CMD_TypeDef;

extern const char AT_CMD_END_MARKER;
extern const char INPUT_TEXT_MARKER;
extern const char* RECEIVE_DATA_MARKER;
extern const char* SIMCARD_NOT_READY;
extern const char* TCP_CON_CLOSED;

extern const CMD_TypeDef SIM800_Sms_Send[COMMANDS_SMS_SIZE];
extern const CMD_TypeDef SIM800_Client_Cmd[COMMANDS_CLIENT_SIZE];
extern const CMD_TypeDef SIM800_Push_Cmd[COMMANDS_PUSH_SIZE];

#endif //_SIM_CMD_H
