#ifndef _EXTI_H
#define _EXTI_H

#include "../CMSIS/inc/stm32f0xx.h"

#include "../StdPeriph/inc/stm32f0xx_exti.h"
#include "../StdPeriph/inc/stm32f0xx_gpio.h"

#include "../pin/pins.h"

#define EXTI0			&GEXTI_LINE0
#define EXTI1			&GEXTI_LINE1
#define EXTI6			&GEXTI_LINE6

#define EXTI_CALLBACK_COUNT			3

typedef void (*ExtiIRQHandler)(void* context);

/*
	typedef enums and structs for exti
*/
typedef enum Exti_trigger_enum
{
	RISING = EXTI_Trigger_Rising,
	FALLING = EXTI_Trigger_Falling,
	RISING_FALLING = EXTI_Trigger_Rising_Falling
} Exti_trigger;

typedef enum Exti_mode_enum
{
	EXTI_Interrupt = EXTI_Mode_Interrupt,
	EXTI_Event = EXTI_Mode_Event
} Exti_mode;

typedef struct Exti_struct
{
	uint32_t				ExtiLine;
	ExtiIRQHandler			Callbacks[EXTI_CALLBACK_COUNT];
	uint8_t					CallbackCounter;
	void*					Context;
	Exti_trigger			Trigger;
	Exti_mode				ExtiMode;
} Exti_TypeDef;

void Exti_Init(Exti_TypeDef* exti, Pins_TypeDef* pin, void (*callback)(void* context), void* context);
void Exti_DeInit(Exti_TypeDef* exti);
void Exti_On(Exti_TypeDef* exti, Exti_trigger trigger, Exti_mode mode, uint8_t priority);
void Exti_Off(Exti_TypeDef* exti);

/*
	//	Private methods
*/
uint8_t GetIRQForExti(Exti_TypeDef* exti);

/*
	extern instances of TIMERS
*/
extern Exti_TypeDef GEXTI_LINE0;
extern Exti_TypeDef GEXTI_LINE1;
extern Exti_TypeDef GEXTI_LINE6;


#endif