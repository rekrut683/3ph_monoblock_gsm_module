#include "exti.h"
#include "nvic.h"

void EXTI0_1_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line0);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI0).Callbacks[i])
			{
				((*EXTI0).Callbacks[i])((*EXTI0).Context);
			}
		}
	}
	if (EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line1);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI1).Callbacks[i])
			{
				((*EXTI1).Callbacks[i])((*EXTI1).Context);
			}
		}
	}
}

void EXTI2_3_IRQHandler(void)
{
  	
}

void EXTI4_15_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line6) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line6);
		for (uint8_t i = 0; i < EXTI_CALLBACK_COUNT; i++)
		{
			if ((*EXTI6).Callbacks[i])
			{
				((*EXTI6).Callbacks[i])((*EXTI6).Context);
			}
		}
	}
}

void Exti_Init(Exti_TypeDef* exti, Pins_TypeDef* pin, void (*callback)(void* context), void* context)
{
  	/* Enable SYSCFG clock */
  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
	GPIOS_TypeDef* port = pin->Port;
	uint8_t portSource = port->PortSource;
	//GPIO_EXTILineConfig(portSource, pin->PinSource);
  	SYSCFG_EXTILineConfig(portSource, pin->ExtiPinSource);
	//GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource9);
	//GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource7);
	//GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource8);
	if (exti->CallbackCounter == EXTI_CALLBACK_COUNT)
	{
		exti->CallbackCounter = 0;
	}
	exti->Callbacks[exti->CallbackCounter] = callback;
	exti->Context = context;
}

void Exti_DeInit(Exti_TypeDef* exti)
{

}

void Exti_On(Exti_TypeDef* exti, Exti_trigger trigger, Exti_mode mode, uint8_t priority)
{
	exti->Trigger = trigger;
	exti->ExtiMode = mode;
	
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = exti->ExtiLine;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = (EXTIMode_TypeDef)exti->ExtiMode;
	EXTI_InitStruct.EXTI_Trigger = (EXTITrigger_TypeDef)exti->Trigger;
	EXTI_Init(&EXTI_InitStruct);

	uint8_t irq = GetIRQForExti(exti);
	
	if(!NVIC_Check(irq))
	{
		NVIC_Add(irq);
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = irq;
		//NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
}

void Exti_Off(Exti_TypeDef* exti)
{
	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = exti->ExtiLine;
	EXTI_InitStruct.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStruct);
}

/******************PRIVATE METHODS*******************/
/*
	Get IRQ Type for NVIC
*/
uint8_t GetIRQForExti(Exti_TypeDef* exti)
{
	if (exti->ExtiLine == EXTI_Line0)
	{
		return EXTI0_1_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line1)
	{
		return EXTI0_1_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line2)
	{
		return EXTI2_3_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line3)
	{
		return EXTI2_3_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line4)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line5)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line6)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line7)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line8)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line9)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line10)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line11)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line12)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line13)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line14)
	{
		return EXTI4_15_IRQn;
	}
	else if (exti->ExtiLine == EXTI_Line15)
	{
		return EXTI4_15_IRQn;
	}
	else
	{
		return 0xFF;
	}
}