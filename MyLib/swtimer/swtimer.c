#include "swtimer.h"

SWTimerControl_TypeDef SwTimerControl;

void SWTimer_Interrupt()
{
	for (int i = 0; i < SwTimerControl.SwTimersCount; i++)
	{
		if (SwTimerControl.SwTimers[i] &&
			SwTimerControl.SwTimers[i]->IsEnabled && 
			SwTimerControl.SwTimers[i]->Callback)
		{
			if ((SysTimer_Millis() - SwTimerControl.SwTimers[i]->StartValue) >= SwTimerControl.SwTimers[i]->InterruptValue)
			{
			  	SwTimerControl.SwTimers[i]->StartValue = SysTimer_Millis();
				SwTimerControl.SwTimers[i]->Callback(SwTimerControl.SwTimers[i]->Context);
			}
		}
	}
}

static inline void addToSysTimerList(SWTimer_TypeDef* swtimer)
{
  	if (!SystemTimer.TickCallback)
	{
	  	SystemTimer.TickCallback = SWTimer_Interrupt;
	}
	SwTimerControl.SwTimersCount++;
	if (SwTimerControl.SwTimersCount >= MAX_SWTIMERS)
	{
		return;
	}
	SwTimerControl.SwTimers[SwTimerControl.SwTimersCount - 1] = swtimer;
}

static void SWTimer_DeleteFromSysTimerList(SWTimer_TypeDef* swtimer)
{
	int8_t pos = -1;
	// ���� ������� ������ ���� � ������
	if (SwTimerControl.SwTimersCount > 0)
	{
		// ��������� ��� ������� �� ������
		for (int i = 0; i < SwTimerControl.SwTimersCount; i++)
		{
			// ���� ����� ������ ������, ��������� ��� ������� � ������
			if (SwTimerControl.SwTimers[i] == swtimer)
			{
				pos = i;
				break;
			}
		}
		if (pos != -1)
		{
			// ���� ������� ������� �� � ����� ������
			if (pos < SwTimerControl.SwTimersCount - 1)
			{
				// �� ������ ������������
				for (int i = pos; i < SwTimerControl.SwTimersCount - 1; i++)
				{
					SwTimerControl.SwTimers[i] = SwTimerControl.SwTimers[i + 1];
				}
			}
			// �������� ���-�� �������� �� ��������
			SwTimerControl.SwTimersCount--;
		}
	}
}

void SWTimer_Init(SWTimer_TypeDef* swtimer)
{
	addToSysTimerList(swtimer);
}

void SWTimer_DeInit(SWTimer_TypeDef* swtimer)
{
	SWTimer_Stop(swtimer);
	SWTimer_DeleteFromSysTimerList(swtimer);
}

void SWTimer_Start(SWTimer_TypeDef* swtimer, uint32_t ticks)
{
  	swtimer->InterruptValue = ticks;
	swtimer->StartValue = SysTimer_Millis();
	swtimer->IsEnabled = 1;
}

void SWTimer_SubscribeCallback(SWTimer_TypeDef* swtimer, void* context, void (*callback)(void*))
{
	swtimer->Context = context;
	swtimer->Callback = callback;
}

void SWTimer_UnsubscribeCallback(SWTimer_TypeDef* swtimer)
{
	swtimer->Callback = 0;
}

void SWTimer_Stop(SWTimer_TypeDef* swtimer)
{
	swtimer->IsEnabled = 0;
}

void SWTimer_Reset(SWTimer_TypeDef* swtimer)
{
	swtimer->StartValue = SysTimer_Millis();
}

void SWTimer_Pause(SWTimer_TypeDef* swtimer)
{
	swtimer->IsEnabled = 0;
	swtimer->backupValue = swtimer->InterruptValue - (SysTimer_Millis() - swtimer->StartValue);
}

void SWTimer_Resume(SWTimer_TypeDef* swtimer)
{
	swtimer->InterruptValue = SysTimer_Millis() + swtimer->backupValue;
	swtimer->IsEnabled = 1;
}
