#include "pins.h"

extern Pins_TypeDef PA0 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_0,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource0,
	.GpioPinSource	= GPIO_PinSource0,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_0
};

extern Pins_TypeDef PA1 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_1,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource1,
	.GpioPinSource	= GPIO_PinSource1,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_1
};

extern Pins_TypeDef PA2 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_2,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource2,
	.GpioPinSource	= GPIO_PinSource2,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_2,
	.AF_func	= GPIO_AF_1
};

extern Pins_TypeDef PA3 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_3,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource3,
	.GpioPinSource	= GPIO_PinSource3,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_3,
	.AF_func	= GPIO_AF_1
};

extern Pins_TypeDef PA4 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_4,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource4,
	.GpioPinSource	= GPIO_PinSource4,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_4
};

extern Pins_TypeDef PA5 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_5,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource5,
	.GpioPinSource	= GPIO_PinSource5,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_5,
};

extern Pins_TypeDef PA6 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_6,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource6,
	.GpioPinSource	= GPIO_PinSource6,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_6
};

extern Pins_TypeDef PA7 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_7,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource7,
	.GpioPinSource	= GPIO_PinSource7,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_7
};

extern Pins_TypeDef PA8 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_8,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource8,
	.GpioPinSource	= GPIO_PinSource8,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PA9 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_9,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource9,
	.GpioPinSource	= GPIO_PinSource9,
	.IsAdcPin	= 0,
	.AF_func = GPIO_AF_1
};

extern Pins_TypeDef PA10 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_10,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource10,
	.GpioPinSource	= GPIO_PinSource10,
	.IsAdcPin	= 0,
	.AF_func = GPIO_AF_1
};

extern Pins_TypeDef PA11 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_11,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource11,
	.GpioPinSource	= GPIO_PinSource11,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PA12 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_12,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource12,
	.GpioPinSource	= GPIO_PinSource12,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PA13 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_13,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource13,
	.GpioPinSource	= GPIO_PinSource13,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PA14 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_14,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource14,
	.GpioPinSource	= GPIO_PinSource14,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PA15 = 
{
	.Port		= PORTA,
	.Pin		= GPIO_Pin_15,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource15,
	.GpioPinSource	= GPIO_PinSource15,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB0 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_0,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource0,
	.GpioPinSource	= GPIO_PinSource0,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_8
};

extern Pins_TypeDef PB1 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_1,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource1,
	.GpioPinSource	= GPIO_PinSource1,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_9
};

extern Pins_TypeDef PB2 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_2,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource2,
	.GpioPinSource	= GPIO_PinSource2,
	.IsAdcPin	= 1,
	.AdcChannel = ADC_Channel_9
};

extern Pins_TypeDef PB3 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_3,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource3,
	.GpioPinSource	= GPIO_PinSource3,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB4 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_4,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource4,
	.GpioPinSource	= GPIO_PinSource4,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB5 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_5,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource5,
	.GpioPinSource	= GPIO_PinSource5,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB6 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_6,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource6,
	.GpioPinSource	= GPIO_PinSource6,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB7 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_7,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource7,
	.GpioPinSource	= GPIO_PinSource7,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB8 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_8,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource8,
	.GpioPinSource	= GPIO_PinSource8,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB9 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_9,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource9,
	.GpioPinSource	= GPIO_PinSource9,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB10 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_10,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource10,
	.GpioPinSource	= GPIO_PinSource10,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB11 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_11,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource11,
	.GpioPinSource	= GPIO_PinSource11,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB12 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_12,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource12,
	.GpioPinSource	= GPIO_PinSource12,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB13 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_13,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource13,
	.GpioPinSource	= GPIO_PinSource13,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB14 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_14,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource14,
	.GpioPinSource	= GPIO_PinSource14,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PB15 = 
{
	.Port		= PORTB,
	.Pin		= GPIO_Pin_15,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource15,
	.GpioPinSource	= GPIO_PinSource15,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC0 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_0,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource0,
	.GpioPinSource	= GPIO_PinSource0,
	.IsAdcPin	= 0
};
extern Pins_TypeDef PC1 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_1,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource1,
	.GpioPinSource	= GPIO_PinSource1,
	.IsAdcPin	= 0
};
extern Pins_TypeDef PC2 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_2,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource2,
	.GpioPinSource	= GPIO_PinSource2,
	.IsAdcPin	= 0
};
extern Pins_TypeDef PC3 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_3,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource3,
	.GpioPinSource	= GPIO_PinSource3,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC4 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_4,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource4,
	.GpioPinSource	= GPIO_PinSource4,
	.IsAdcPin	= 0,
	.AF_func	= GPIO_AF_1
};

extern Pins_TypeDef PC5 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_5,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource5,
	.GpioPinSource	= GPIO_PinSource5,
	.IsAdcPin	= 0,
	.AF_func	= GPIO_AF_1
};

extern Pins_TypeDef PC6 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_6,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource6,
	.GpioPinSource	= GPIO_PinSource6,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC7 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_7,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource7,
	.GpioPinSource	= GPIO_PinSource7,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC8 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_8,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource8,
	.GpioPinSource	= GPIO_PinSource8,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC9 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_9,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource9,
	.GpioPinSource	= GPIO_PinSource9,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC10 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_10,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource10,
	.GpioPinSource	= GPIO_PinSource10,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC11 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_11,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource11,
	.GpioPinSource	= GPIO_PinSource11,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC12 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_12,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource12,
	.GpioPinSource	= GPIO_PinSource12,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC13 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_13,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource13,
	.GpioPinSource	= GPIO_PinSource13,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC14 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_14,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource14,
	.GpioPinSource	= GPIO_PinSource14,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PC15 = 
{
	.Port		= PORTC,
	.Pin		= GPIO_Pin_15,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource15,
	.GpioPinSource	= GPIO_PinSource15,
	.IsAdcPin	= 0
};

extern Pins_TypeDef PD2 = 
{
	.Port		= PORTD,
	.Pin		= GPIO_Pin_2,
	.Speed		= LOW_SPEED,
	.Mode		= GPIO_Mode_IN,
	.ExtiPinSource	= EXTI_PinSource2,
	.GpioPinSource	= GPIO_PinSource2,
	.IsAdcPin	= 0
};