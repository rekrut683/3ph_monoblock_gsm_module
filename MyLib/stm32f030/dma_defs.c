#include "dma.h"

extern Dma_TypeDef GDMA1 =
{
	.RCC_Bus		= RCC_AHBPeriph_DMA1,
	.RCC_Register	= &RCC->AHBENR
};

extern DmaChannel_TypeDef GDMA_CHANNEL1 =
{
	.DmaChannel		= DMA1_Channel1
};

extern DmaChannel_TypeDef GDMA_CHANNEL2 =
{
	.DmaChannel		= DMA1_Channel2
};

extern DmaChannel_TypeDef GDMA_CHANNEL3 =
{
	.DmaChannel		= DMA1_Channel3
};

extern DmaChannel_TypeDef GDMA_CHANNEL4 =
{
	.DmaChannel		= DMA1_Channel4
};

extern DmaChannel_TypeDef GDMA_CHANNEL5 =
{
	.DmaChannel		= DMA1_Channel5
};