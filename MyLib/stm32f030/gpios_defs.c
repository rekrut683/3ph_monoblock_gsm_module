#include "gpioss.h"

extern GPIOS_TypeDef GPORTA = 
{
	.GPIO			= GPIOA,
	.RCC_Bus		= RCC_AHBPeriph_GPIOA,
	.RCC_Register	= &RCC->AHBENR,
	.PortSource		= EXTI_PortSourceGPIOA
};

extern GPIOS_TypeDef GPORTB =
{
	.GPIO			= GPIOB,
	.RCC_Bus		= RCC_AHBPeriph_GPIOB,
	.RCC_Register	= &RCC->AHBENR,
	.PortSource		= EXTI_PortSourceGPIOB
};

extern GPIOS_TypeDef GPORTC =
{
	.GPIO			= GPIOC,
	.RCC_Bus		= RCC_AHBPeriph_GPIOC,
	.RCC_Register	= &RCC->AHBENR,
	.PortSource		= EXTI_PortSourceGPIOC
};

extern GPIOS_TypeDef GPORTD =
{
	.GPIO			= GPIOD,
	.RCC_Bus		= RCC_AHBPeriph_GPIOD,
	.RCC_Register	= &RCC->AHBENR,
	.PortSource		= EXTI_PortSourceGPIOD
};

extern GPIOS_TypeDef GPORTF =
{
	.GPIO			= GPIOF,
	.RCC_Bus		= RCC_AHBPeriph_GPIOF,
	.RCC_Register	= &RCC->AHBENR,
	.PortSource		= EXTI_PortSourceGPIOF
};

/*
extern GPIOS_TypeDef GGPIO_AF = 
{
	.RCC_Bus		= RCC_APB2Periph_AFIO,
	.RCC_Register	= &RCC->APB2ENR
};
*/