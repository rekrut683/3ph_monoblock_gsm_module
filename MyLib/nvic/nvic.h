#ifndef _NVIC_H
#define _NVIC_H

#include <stdint.h>

#define IRQ_Collection						50

static uint8_t NVIC_Irq[IRQ_Collection];
volatile static uint8_t currentSize = 0;

void NVIC_Add(uint8_t irq);
void NVIC_Delete(uint8_t irq);
uint8_t NVIC_Check(uint8_t irq);


#endif