#ifndef _UART_H
#define _UART_H

#include <stdint.h>
#include "../StdPeriph/inc/stm32f0xx_usart.h"
#include "../pin/pins.h"
#include "../dma/dma.h"

#define USART_1				&GUSART1
#define USART_2				&GUSART2
#define USART_3				&GUSART3
#define USART_4				&GUSART4
#define USART_5				&GUSART5
#define USART_6				&GUSART6

#define USART_TX_BUFFER_SIZE						32

typedef void (*OnReceiveUsartCallbackType)(void*,uint8_t);
static uint8_t TxBuffer[USART_TX_BUFFER_SIZE];

typedef struct Callback_struct
{
	OnReceiveUsartCallbackType callback;
	void* context;
} CallbackUartWrap;

typedef enum Uart_WordLength_enum
{
	UART_WORDLENGTH_8B			= USART_WordLength_8b,
	UART_WORDLENGTH_9B			= USART_WordLength_9b
} USART_WORDLENGTH;

typedef enum Uart_StopBits_enum
{
	USART_STOPBITS_1			= USART_StopBits_1,
	USART_STOPBITS_2			= USART_StopBits_2,
	USART_STOPBITS_1_5			= USART_StopBits_1_5
} USART_STOPBITS;

typedef enum Uart_mode_enum
{
	USART_MODE_RX				= USART_Mode_Rx,
	USART_MODE_TX				= USART_Mode_Tx
} USART_MODE;

typedef enum Uart_FlowControl_enum
{
	USART_FLOWCONTROL_NONE		= USART_HardwareFlowControl_None,
	USART_FLOWCONTROL_RTS		= USART_HardwareFlowControl_RTS,
	USART_FLOWCONTROL_CTS		= USART_HardwareFlowControl_CTS,
	USART_FLOWCONTROL_RTS_CTS	= USART_HardwareFlowControl_RTS_CTS
} USART_FLOWCONTROL;

typedef struct Uart_struct
{
	// uart periph
	USART_TypeDef*					Usart;
	// uart pins
	Pins_TypeDef*					Tx_pin;
	Pins_TypeDef*					Rx_pin;
	Pins_TypeDef*					Ck_pin;
	Pins_TypeDef*					CTS_pin;
	Pins_TypeDef*					RTS_pin;
	// uart settings
	uint32_t						BaudRate;
	USART_WORDLENGTH				UsartWordlength;
	USART_STOPBITS					UsartStopbits;
	USART_MODE						UsartMode;
	USART_FLOWCONTROL				UsartFlowControl;
	// uart callbacks
	CallbackUartWrap				OnReceiveCallbacks[5];
	uint8_t							CountCallbacks;
	// uart clock registers
	uint32_t	 					RCC_Bus;
	volatile uint32_t*				RCC_Register;
	// uart dma
	DmaChannel_TypeDef*				DmaTxChannel;
	DmaChannel_TypeDef*				DmaRxChannel;
	volatile uint16_t*				UsartTxAddress;
	// uart flags & state
	volatile uint8_t				TxBusy;
} Uart_TypeDef;

uint8_t GetIRQForUsart(Uart_TypeDef* uart);

void Usart_Enable(Uart_TypeDef* uart);
void Usart_Disable(Uart_TypeDef* uart);

void Usart_Init(Uart_TypeDef* usart, uint32_t baudrate);
void Usart_ClearTxBuffer(Uart_TypeDef* usart);

void Usart_Send(Uart_TypeDef* usart, const uint8_t* data);
void Usart_SubscribeReceive(Uart_TypeDef* usart, void* context, void(*callback)(void*, uint8_t));

//void UsartTxDma_Init(Uart_TypeDef* usart, uint32_t buffer, uint32_t bufferSize);
void UsartTxDma_Init(Uart_TypeDef* usart);
void UsartTxDma_Send(Uart_TypeDef* usart, const uint8_t* data);

extern Uart_TypeDef GUSART1;
extern Uart_TypeDef GUSART2;
extern Uart_TypeDef GUSART3;
extern Uart_TypeDef GUSART4;
extern Uart_TypeDef GUSART5;
extern Uart_TypeDef GUSART6;

#endif