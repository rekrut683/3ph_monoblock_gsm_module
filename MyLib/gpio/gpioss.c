#include "gpioss.h"

void Port_Enable(GPIOS_TypeDef* gpio)
{
  	//uint32_t a = RCC->APB2ENR;
	*gpio->RCC_Register |= gpio->RCC_Bus;
	//uint32_t b = RCC->APB2ENR;
}

void Port_Disable(GPIOS_TypeDef* gpio)
{
	*gpio->RCC_Register &= ~gpio->RCC_Bus;
}