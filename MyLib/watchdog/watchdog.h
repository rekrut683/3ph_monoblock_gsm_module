#ifndef _WATCHDOG_H
#define _WATCHDOG_H

#include "../../StdPeriph/inc/stm32f0xx_iwdg.h"

void Watchdog_Enable(uint8_t prescaler, uint16_t reloadValue);
void Watchdog_Reset();

#endif