#ifndef _DMA_H
#define _DMA_H

#include "../StdPeriph/inc/stm32f0xx_dma.h"

#include <stdint.h>

#define DMA_1			&GDMA1
#define DMA1_CHANNEL1	&GDMA_CHANNEL1
#define DMA1_CHANNEL2	&GDMA_CHANNEL2
#define DMA1_CHANNEL3	&GDMA_CHANNEL3
#define DMA1_CHANNEL4	&GDMA_CHANNEL4
#define DMA1_CHANNEL5	&GDMA_CHANNEL5

#define DMA_CALLBACK_COUNT					5

typedef void (*OnDmaInterrupt)(void* context);

typedef enum DMA_DIRECTION_ENUM
{
	DMA_DIR_TO_PERIPH = DMA_DIR_PeripheralDST,
	DMA_DIR_FROM_PERIPH = DMA_DIR_PeripheralSRC
} DMA_DIRECTION;

typedef enum DMA_MODE_ENUM
{
	DMA_MODE_CIRCULAR = DMA_Mode_Circular,
	DMA_MODE_NORMAL = DMA_Mode_Normal
} DMA_MODE;

typedef enum DMA_MEMORY_SIZE_ENUM
{
	DMA_MEMORY_SIZE_BYTE = DMA_MemoryDataSize_Byte,
	DMA_MEMORY_SIZE_HALFWORD = DMA_MemoryDataSize_HalfWord,
	DMA_MEMORY_SIZE_WORD = DMA_MemoryDataSize_Word
} DMA_MEMORY_SIZE;

typedef enum DMA_PERIPH_SIZE_ENUM
{
	DMA_PERIPH_SIZE_BYTE = DMA_PeripheralDataSize_Byte,
	DMA_PERIPH_SIZE_HALFWORD = DMA_PeripheralDataSize_HalfWord,
	DMA_PERIPH_SIZE_WORD = DMA_PeripheralDataSize_Word
} DMA_PERIPHERAL_DATASIZE;

typedef struct Dma_struct
{
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
} Dma_TypeDef;

typedef struct CallbackWrap_struct
{
	void* context;
	OnDmaInterrupt callback;
} CallbackDmaWrap;

typedef struct DmaChannel_struct
{
	DMA_Channel_TypeDef *	DmaChannel;
	uint32_t				BufferSize;
	DMA_DIRECTION			Direction;
	DMA_MEMORY_SIZE			MemorySize;
	DMA_PERIPHERAL_DATASIZE	PeriphDataSize;
	uint32_t				MemoryAddress;
	uint32_t				MemoryDataSize;
	DMA_MODE				DmaMode;
	uint32_t				PeriphAddress;
	CallbackDmaWrap			OnDmaComplete[DMA_CALLBACK_COUNT];
	CallbackDmaWrap			OnDmaError[DMA_CALLBACK_COUNT];
	CallbackDmaWrap			OnHalfTransfer[DMA_CALLBACK_COUNT];
} DmaChannel_TypeDef;

void Dma_Enable(Dma_TypeDef* dma);
void Dma_Disable(Dma_TypeDef* dma);

void DmaChannel_Init(	DmaChannel_TypeDef* channel,
						uint32_t bufSize,
						uint32_t buffer,
						DMA_DIRECTION direction,
						DMA_MEMORY_SIZE	MemorySize,
						DMA_PERIPHERAL_DATASIZE	PeriphDataSize,
						DMA_MODE mode,
						uint32_t periph
					);

void DmaChannel_Start(DmaChannel_TypeDef* channel);
void DmaChannel_Stop(DmaChannel_TypeDef* channel);


void DmaChannel_SubscribeCompleteInterrupt(DmaChannel_TypeDef*,void*,void (*callback)(void*));
void DmaChannel_SubscribeErrorInterrupt(DmaChannel_TypeDef*, void*, void (*callback)(void*));
void DmaChannel_SubscribeHalfTransferInterrupt(DmaChannel_TypeDef*, void*, void (*callback)(void*));

extern Dma_TypeDef GDMA1;
extern DmaChannel_TypeDef GDMA_CHANNEL1;
extern DmaChannel_TypeDef GDMA_CHANNEL2;
extern DmaChannel_TypeDef GDMA_CHANNEL3;
extern DmaChannel_TypeDef GDMA_CHANNEL4;
extern DmaChannel_TypeDef GDMA_CHANNEL5;

#endif