#ifndef _ADC_H
#define _ADC_H

#include "../CMSIS/inc/stm32f0xx.h"
#include "../StdPeriph/inc/stm32f0xx_adc.h"
#include "../StdPeriph/inc/stm32f0xx_misc.h"
#include "../CMSIS/inc/system_stm32f0xx.h"

#include "../pin/pins.h"
#include "../dma/dma.h"

#include <stdint.h>
#include <stdio.h>

#define ADC_1		&GADC1

#define	MAX_ADC_CHANNELS		15

typedef void (*OnConversionAdcCallbackType)(void*, uint16_t);

typedef struct Callback_struct_ADC
{
	OnConversionAdcCallbackType callback;
	void* context;
} CallbackAdcWrap;

typedef struct ADC_Channel
{
	uint8_t				Number;
	uint16_t				Ain;
	CallbackAdcWrap		ConversionCallback[5];
} Adc_Channel_TypeDef;

typedef struct ADC_Struct
{
	ADC_TypeDef*			Adc;
	uint8_t					CountChannels;
	Adc_Channel_TypeDef		Channels[MAX_ADC_CHANNELS];
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
	DmaChannel_TypeDef*		DmaChannel;
	volatile uint32_t*		AdcAddress;
} Adc_TypeDef;

void Adc_Init(Adc_TypeDef* adc, uint32_t divider, uint8_t numberChannels);
void Adc_SetChannel(Adc_TypeDef* adc, uint16_t chan, uint8_t samples);
void Adc_Start(Adc_TypeDef* adc);
void Adc_Stop(Adc_TypeDef* adc);
void Adc_EnableIRQ(Adc_TypeDef* adc);
void Adc_DisableIRQ(Adc_TypeDef* adc);

void AdcDma_Init(Adc_TypeDef* adc, uint32_t buffer, uint32_t bufferSize);
void AdcDma_Start(Adc_TypeDef* adc);

void Adc_SubscribeCallback(Adc_TypeDef* adc, uint8_t number, void* context, void(*callback)(void*, uint16_t));

extern Adc_TypeDef GADC1;

#endif