#include "pins.h"

void Pin_Init(Pins_TypeDef* pin, GPIOMode_TypeDef mode, Pins_speed speed, Pins_OType otype, Pins_IType itype)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = pin->Pin;

    pin->Mode = mode;
    GPIO_InitStructure.GPIO_Mode = pin->Mode;

    pin->Speed = speed;
    GPIO_InitStructure.GPIO_Speed = pin->Speed;

    pin->OType = otype;
    GPIO_InitStructure.GPIO_OType = pin->OType;

    pin->IType = itype;
    GPIO_InitStructure.GPIO_PuPd = pin->IType;

    GPIO_Init(pin->Port->GPIO, &GPIO_InitStructure);
}

void Pin_Init_AF(Pins_TypeDef* pin, GPIOMode_TypeDef mode, Pins_speed speed, Pins_OType otype, Pins_IType itype, uint8_t AF_num)
{
  	GPIO_PinAFConfig(pin->Port->GPIO, pin->GpioPinSource, AF_num);
	Pin_Init(pin, mode, speed, otype, itype);
}

void Pin_Set(Pins_TypeDef* pin)
{
	GPIO_SetBits(pin->Port->GPIO, pin->Pin);
}

void Pin_Clear(Pins_TypeDef* pin)
{
	GPIO_ResetBits(pin->Port->GPIO, pin->Pin);
}

void Pin_Toggle(Pins_TypeDef* pin)
{
	uint8_t read = GPIO_ReadInputDataBit(pin->Port->GPIO, pin->Pin);
	if(!read)
	{
		Pin_Set(pin);
	}
	else
	{
		Pin_Clear(pin);
	}
}

uint8_t Pin_Read(Pins_TypeDef* pin)
{
	return GPIO_ReadInputDataBit(pin->Port->GPIO, pin->Pin);
}