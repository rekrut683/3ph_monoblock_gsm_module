#ifndef _PINS_H
#define _PINS_H

#include "../../StdPeriph/inc/stm32f0xx_gpio.h"

#include "../gpio/gpioss.h"

#define A0		&PA0
#define A1		&PA1
#define A2		&PA2
#define A3		&PA3
#define A4		&PA4
#define A5		&PA5
#define A6		&PA6
#define A7		&PA7
#define A8		&PA8
#define A9		&PA9
#define A10		&PA10
#define A11		&PA11
#define A12		&PA12
#define A13		&PA13
#define A14		&PA14
#define A15		&PA15

#define B0		&PB0
#define B1		&PB1
#define B2              &PB2
#define B3		&PB3
#define B4		&PB4
#define B5		&PB5
#define B6		&PB6
#define B7		&PB7
#define B8		&PB8
#define B9		&PB9
#define B10		&PB10
#define B11		&PB11
#define B12		&PB12
#define B13		&PB13
#define B14		&PB14
#define B15		&PB15

#define C0              &PC0
#define C1              &PC1
#define C2              &PC2
#define C3              &PC3
#define C4              &PC4
#define C5              &PC5
#define C6              &PC6
#define C7              &PC7
#define C8              &PC8
#define C9              &PC9
#define C10             &PC10
#define C11             &PC11
#define C12             &PC12
#define C13		&PC13
#define C14             &PC14
#define C15             &PC15

#define D2              &PD2

#define F0              &PF0
#define F1              &PF1
#define F4              &PF2
#define F5              &PF3
#define F6              &PF4
#define F7              &PF5


/*
//	TypeDef structs for gpio pin's
*/
typedef enum Pins_speed_enum
{
	LOW_SPEED 			= GPIO_Speed_Level_1,
	MIDDLE_SPEED 		        = GPIO_Speed_Level_2,
	HIGH_SPEED 			= GPIO_Speed_Level_3
} Pins_speed;

typedef enum OutputType_enum
{
  	PUSHPULL	= GPIO_OType_PP,
  	OPENDRAIN	= GPIO_OType_OD
} Pins_OType;

typedef enum InputType_enum
{
  	NOPULL		= GPIO_PuPd_NOPULL,
	PUSH_UP		= GPIO_PuPd_UP,
	PUSH_DOWN	= GPIO_PuPd_DOWN
} Pins_IType;


typedef struct Pins_struct
{
    GPIOS_TypeDef*      Port;
    uint32_t            Pin;
    Pins_speed          Speed;
    GPIOMode_TypeDef    Mode;
    Pins_OType          OType;
    Pins_IType          IType;
    uint8_t             ExtiPinSource;
    uint8_t             GpioPinSource;
    uint8_t             IsAdcPin;
    uint8_t             AdcChannel;
    uint8_t             AF_func;
} Pins_TypeDef;


/*
//	Public methods for gpio pin's
*/
void Pin_Init(Pins_TypeDef* pin, GPIOMode_TypeDef mode, Pins_speed speed, Pins_OType otype, Pins_IType itype);
void Pin_Init_AF(Pins_TypeDef* pin, GPIOMode_TypeDef mode, Pins_speed speed, Pins_OType otype, Pins_IType itype, uint8_t AF_num);

void Pin_Set(Pins_TypeDef* pin);
void Pin_Clear(Pins_TypeDef* pin);
void Pin_Toggle(Pins_TypeDef* pin);

uint8_t Pin_Read(Pins_TypeDef* pin);


/*
//	static instances for pin's Stm32f103
*/
extern Pins_TypeDef PA0;
extern Pins_TypeDef PA1;
extern Pins_TypeDef PA2;
extern Pins_TypeDef PA3;
extern Pins_TypeDef PA4;
extern Pins_TypeDef PA5;
extern Pins_TypeDef PA6;
extern Pins_TypeDef PA7;
extern Pins_TypeDef PA8;
extern Pins_TypeDef PA9;
extern Pins_TypeDef PA10;
extern Pins_TypeDef PA11;
extern Pins_TypeDef PA12;
extern Pins_TypeDef PA13;
extern Pins_TypeDef PA14;
extern Pins_TypeDef PA15;
extern Pins_TypeDef PB0;
extern Pins_TypeDef PB1;
extern Pins_TypeDef PB3;
extern Pins_TypeDef PB4;
extern Pins_TypeDef PB5;
extern Pins_TypeDef PB6;
extern Pins_TypeDef PB7;
extern Pins_TypeDef PB8;
extern Pins_TypeDef PB9;
extern Pins_TypeDef PB10;
extern Pins_TypeDef PB11;
extern Pins_TypeDef PB12;
extern Pins_TypeDef PB13;
extern Pins_TypeDef PB14;
extern Pins_TypeDef PB15;
extern Pins_TypeDef PC0;
extern Pins_TypeDef PC1;
extern Pins_TypeDef PC2;
extern Pins_TypeDef PC3;
extern Pins_TypeDef PC4;
extern Pins_TypeDef PC5;
extern Pins_TypeDef PC6;
extern Pins_TypeDef PC7;
extern Pins_TypeDef PC8;
extern Pins_TypeDef PC9;
extern Pins_TypeDef PC10;
extern Pins_TypeDef PC11;
extern Pins_TypeDef PC12;
extern Pins_TypeDef PC13;
extern Pins_TypeDef PD2;
extern Pins_TypeDef PF0;
extern Pins_TypeDef PF1;
extern Pins_TypeDef PF2;
extern Pins_TypeDef PF3;
extern Pins_TypeDef PF4;
extern Pins_TypeDef PF5;


#endif