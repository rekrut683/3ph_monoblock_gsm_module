#include "timers.h"
#include "stm32f0xx_rcc.h"

/*
	Get IRQ Type for NVIC
*/
uint8_t GetIRQForTimer(TIMER_TypeDef* timer)
{
    return (timer->Timer == TIM3) ? TIM3_IRQn : 0xFF;
}

/*
	Timers Interrupt Handlers
*/

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		for(uint8_t i = 0; i < (*TIMER3).Count_callbacks; i++)
		{
			((*TIMER3).Callbacks[i])((*TIMER3.Context));
		}
	}
}

/*
	Public methods for Timer Struct
*/
void Timer_Enable(TIMER_TypeDef* timer)
{
	*timer->RCC_Register |= timer->RCC_Bus;
}

void Timer_Disable(TIMER_TypeDef* timer)
{
	*timer->RCC_Register &= ~timer->RCC_Bus;
}

void Timer_Init(TIMER_TypeDef* timer, uint32_t prescaler, uint32_t period, Timer_Mode mode)
{	
	TIM_TimeBaseInitTypeDef TIMER_InitStructure;
	TIM_TimeBaseStructInit(&TIMER_InitStructure);
	
	timer->Mode = mode;
	TIMER_InitStructure.TIM_CounterMode = timer->Mode;
	
	timer->Prescaler = prescaler;
	TIMER_InitStructure.TIM_Prescaler = timer->Prescaler;
	
	timer->Period = period;
	TIMER_InitStructure.TIM_Period = period;
	
	TIM_TimeBaseInit(timer->Timer, &TIMER_InitStructure);
	
	
	
	//TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
	//TIM_Cmd(TIM4, ENABLE);
}

void Timer_Start(TIMER_TypeDef* timer)
{
	TIM_Cmd(timer->Timer, ENABLE);
	TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
}

void Timer_Stop(TIMER_TypeDef* timer)
{
	TIM_Cmd(timer->Timer, DISABLE);
}

void Timer_SetCounterValue(TIMER_TypeDef* timer, uint32_t value)
{
	TIM_SetCounter(timer->Timer, value);
}

uint32_t Timer_GetCounterValue(TIMER_TypeDef* timer)
{
	return TIM_GetCounter(timer->Timer);
}

void Timer_SetPeriodValue(TIMER_TypeDef* timer, uint32_t value)
{
	timer->Timer->ARR = value;
}

void Timer_IRQSubscribe(TIMER_TypeDef* timer, void (*callback)(void* context), void* context)
{
	timer->Count_callbacks += 1;
	if(timer->Count_callbacks == 5)
	{
		timer->Count_callbacks = 4;
		return;
	}
	timer->Callbacks[timer->Count_callbacks-1] = callback;
	timer->Context = context;
}

void Timer_IRQUnsubscribe(TIMER_TypeDef* timer, void (*callback)(void* context))
{
	//������� ������� � ������� ���������� ��������
	int pos = -1;
	for(int i = 0; i < timer->Count_callbacks; i++)
	{
		if(callback == timer->Callbacks[i])
		{
			pos = i;
			break;
		}
	}
	//��� �� ���������� � ���-�� ���������� ��������� >0
	if(pos != -1 && timer->Count_callbacks)
	{
		//���� ��� �� ��������� � ������ �������, �� ����� ��������� ��� ���������
		if(timer->Count_callbacks > pos+1)
		{
			int count = pos+1 - timer->Count_callbacks;
			for(int i = pos; i < pos+count; i++)
			{
				timer->Callbacks[i] = timer->Callbacks[i+1];
			}
		}
	}
	// ���� ��������
}

void Timer_IRQEnable(TIMER_TypeDef* timer)
{
    //NVIC setup
    uint8_t irq_Ch = GetIRQForTimer(timer);
    if (irq_Ch != 0xFF)
    {
        NVIC_InitTypeDef NVIC_InitStruct;
        NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
        NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
        //NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
        //NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStruct);
    }
	TIM_ITConfig(timer->Timer, TIM_IT_Update, ENABLE);
}

void Timer_IRQDisable(TIMER_TypeDef* timer)
{
    //NVIC setup
    uint8_t irq_Ch = GetIRQForTimer(timer);
    if (irq_Ch != 0xFF)
    {
        NVIC_InitTypeDef NVIC_InitStruct;
        NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
        //NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
        //NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStruct.NVIC_IRQChannelCmd = DISABLE;
        NVIC_Init(&NVIC_InitStruct);
    }
    TIM_ITConfig(timer->Timer, TIM_IT_Update, DISABLE);
}