#ifndef _GENERIC_BUFFER_H
#define _GENERIC_BUFFER_H

#include <stdio.h>
#include <stdint.h>

#define GENERIC_BUFFER_SIZE	20

typedef struct SMS_Instance_Struct
{
  	uint8_t* number;
	uint8_t* message;
} SMS_Instance;

typedef struct
{
  	SMS_Instance* buffer;
	uint16_t idxIn;
	uint16_t idxOut;
	uint16_t size;
} SmsBuffer_t;

uint8_t initSMSBuffer(SmsBuffer_t* rBuffer, uint16_t size);
void deinitSMSBuffer(SmsBuffer_t* rBuffer);
void smsBuffer_put(SmsBuffer_t* rBuffer, SMS_Instance data);
SMS_Instance smsBuffer_pop(SmsBuffer_t* rBuffer);
uint16_t smsBuffer_getCount(SmsBuffer_t* rBuffer);

#endif //_GENERIC_BUFFER_H
