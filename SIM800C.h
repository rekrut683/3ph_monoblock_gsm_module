#ifndef _SIM800C_H
#define _SIM800C_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "MyLib/pin/pins.h"
#include "MyLib/uart/uart.h"
#include "MyLib/swtimer/swtimer.h"
#include "RingBuffer.h"
#include "SmsBuffer.h"
#include "SimCmd.h"
#include "PushMsgBuffer.h"
#include "PacketBuffer.h"
#include "meter_exchange.h"

#define FULL_LOG_SIM800
#define DOT_LOG_SIM800

#define BUFFER_SIZE				256
#define SMS_COUNT				20

#define BAUDRATE				9600

#define DELAY_POWER_UP			500
#define DELAY_RESET_UP			1500
#define DELAY_WAITING_INIT		1000

#define COUNT_OF_ATTEMPT		10

#define TIMEOUT_RESPONSE		3500

//#define APN_NET_BEELINE			"internet.beeline.ru"
//#define APN_NET_MTS				"internet.mts.ru"

//#define APN_NET_MTS				"static.sib"

#define APN_NET_MTS                 "staticip.volga"

//#define APN_NET_MTS				"sk.hes"

//#define APN_UNAME				"mts"
//#define APN_PASS				"mts"
#define APN_UNAME				""
#define APN_PASS				""

//--------SELF LISTEN PORT----------------//
#define LISTEN_PORT				2405

//---------------------------------------//
#define SERVER_ADDR				"46.23.189.41"				//pyramida server
//#define SERVER_PORT				3000					//pyramida server
#define SERVER_PORT				55006
#define USD_BALANCE_MTS			"#100#"
#define USD_BALANCE_BEELINE		"#102#"
#define USD_BALANCE_MEGAFON		"#102#"


//------------PYRAMIDA IP CATCH---------------//
#define SERVER_IP_ADDR			"46.23.189.41"				// promenergo server
#define SERVER_IP_PORT			4500						// promenergo port

//#define SERVER_IP_ADDR			"10.3.245.249"
//#define SERVER_IP_PORT			2407



#define USD_NUMBERTLPH_BEELINE	"*110*10#"

// 
typedef void(*SIM800_OnSim800CallbackEventType)(void*);
typedef void (*SIM800_OnSim800CallbackStateType)(void*,SIM800_ERROR);
typedef void (*SIM800_OnSim800DataReceiveCallbackType)(void*,uint16_t);

typedef enum SIM800_STATUS_RESPONSE_ENUM
{
	SIM800_RESPONSE_GOOD = 0,
	SIM800_RESPONSE_BAD,
	SIM800_RESPONSE_NOT

}SIM800_STATUS_RESPONSE;

typedef enum SIM800_GLOBAL_STATE_ENUM
{
  	SIM800_OFF 			= 0,
	
	SIM800_INIT_PROCESS,
	SIM800_INIT,
	
	SIM800_READY,
	
	SIM800_GPRS_PROCESS,
	SIM800_GPRS_READY,
	
	SIM800_SERVER_INIT_PROCESS,
	SIM800_SERVER_READY,
	
	SIM800_CLIENT_INIT_PROCESS,
	SIM800_CLIENT_READY,

	SIM800_WAITING_MODE,

	SIM800_GASP_INIT_PROCESS,
	SIM800_GASP_READY
} SIM800_GLOBAL_STATE;

typedef enum SIM800_POWER_UP_STATE_ENUM
{
  	SIM800_POWER_OFF 			= 0,

	SIM800_POWER_ON_IDLE,
	SIM800_POWER_ON,

	SIM800_POWER_RESET_IDLE,
	SIM800_POWER_RESET,

	SIM800_POWER_READY_IDLE,
	SIM800_POWER_READY
} SIM800_POWER_UP_STATE;

typedef enum SIM800_PROGRAM_INIT_STATE_ENUM
{
  	SIM800_PROGRAM_OFF			= 0,

	SIM800_AT_CHECK_IDLE,
	SIM800_AT_CHECK,

	SIM800_CHECK_SIM_IDLE,
	SIM800_CHECK_SIM,

	SIM800_CHECK_SIGNAL_IDLE,
	SIM800_CHECK_SIGNAL,

	SIM800_CHECK_BALANCE_IDLE,
	SIM800_CHECK_BALANCE,

	SIM800_BUSYGSM_IDLE,
	SIM800_BUSYGSM,

	SIM800_GETNUMBER_IDLE,
	SIM800_GETNUMBER,

	SIM800_PROGRAM_READY_IDLE,
	SIM800_PROGRAM_READY,

	SIM800_PROGRAM_FAILED		= 20
} SIM800_PROGRAM_INIT_STATE;

typedef enum SIM800_GPRS_INIT_STATE_ENUM
{
  	SIM_GPRS_OFF				= 0,
	
	SIM_GPRS_STATUS_IDLE,
	SIM_GPRS_STATUS,

	SIM_GPRS_RESET_CON_IDLE,
	SIM_GPRS_RESET_CON,

	SIM_GPRS_HW_FLOW_IDLE,
	SIM_GPRS_HW_FLOW,

	SIM_GPRS_SET_HEAD_IDLE,
	SIM_GPRS_SET_HEAD,

	SIM_GPRS_REMOTEI_IDLE,
	SIM_GPRS_REMOTEI,

	SIM_GPRS_IP_STATUS_IDLE,
	SIM_GPRS_IP_STATUS,

	SIM_GPRS_APN_IDLE,
	SIM_GPRS_APN,

	SIM_GPRS_CONTEXT_IDLE,
	SIM_GPRS_CONTEXT,

	SIM_GPRS_GET_IP_IDLE,
	SIM_GPRS_GET_IP,

	SIM_GPRS_READY_IDLE,
	SIM_GPRS_READY,

	SIM_GPRS_FAILED				= 30
	
} SIM800_GPRS_INIT_STATE;

typedef enum SIM800_SERVER_INIT_STATE_ENUM
{
    SIM_SERVER_OFF  = 0,

	SIM_SERVER_RST_CON_IDLE,
	SIM_SERVER_RST_CON,

	SIM_SERVER_START_IDLE,
	SIM_SERVER_START,

	SIM_SERVER_FAILED			= 20
} SIM800_SERVER_INIT_STATE;

typedef enum SIM800_CLIENT_INIT_STATE_ENUM
{
  	SIM_CLIENT_OFF				= 0,

	SIM_CLIENT_RST_CON_IDLE,
	SIM_CLIENT_RST_CON,

	SIM_CLIENT_START_IDLE,
	SIM_CLIENT_START,

	SIM_CLIENT_FAILED			= 10
} SIM800_CLIENT_INIT_STATE;

typedef enum SIM800_SMS_INIT_STATE_ENUM
{
  	SIM_SMS_OFF					= 0,
	SIM_SMS_TEXT_MODE_PROCESS,
	SIM_SMS_TEXT_MODE,
	SIM_SMS_TEL_PROCESS,
	SIM_SMS_TEL,
	SIM_SMS_MESSAGE_PROCESS,
	SIM_SMS_MESSAGE,
	SIM_SMS_END_PROCESS,
	SIM_SMS_END,
	SIM_SMS_FINISH
} SIM800_SMS_INIT_STATE;

typedef enum SIM800_RECEIVE_DATA_STATE_ENUM
{
	SIM_RCV_NOT				= 0,
	SIM_RCV_MARKER,
	SIM_RCV_SEP,
	SIM_RCV_PROCESS,
	SIM_RCV_FINISHED
} SIM800_RECEIVE_DATA_STATE;

typedef enum 
{
	SIM_SEND_IDLE		= 0,
  	SIM_SEND_INIT		= 1,
	SIM_SEND_WAIT_INIT 	= 2,
	SIM_SEND_PROCESS    = 3,
	SIM_SEND_DATA_IDLE  = 4,
	SIM_SEND_DATA,
	SIM_SEND_DATA_OK,

	SIM_SEND_DATA_ERROR			= 30
} SIM800_SEND_DATA_STATE;

typedef enum SIM800_RESPONSE_VALUE_ENUM
{
	SIM_NET_QUALITY				= 0,
	SIM_BALANCE,
	SIM_IP

} SIM800_RESPONSE_VALUE;

typedef enum SIM800_PING_STATE_ENUM
{
	SIM_PING_OFF = 0,
	SIM_PING_IDLE,
	SIM_PING,
	SIM_PING_DONE,

	SIM_PING_IPSTATE_IDLE,
	SIM_PING_IPSTATE,

	SIM_PING_FAILED = 20
} SIM800_PING_STATE;

typedef enum SIM800_PUSH_STATE_ENUM
{
	SIM_PUSH_OFF = 0,

	SIM_PUSH_BACKUP_PREV_STATE_IDLE,
	SIM_PUSH_BACKUP_PREV_STATE,

	SIM_PUSH_SHUTDOWN_CONN_IDLE,
	SIM_PUSH_SHUTDOWN_CONN,

	SIM_PUSH_RESET_STATES_IDLE,
	SIM_PUSH_RESET_STATES,

	SIM_PUSH_CONNECT_IDLE,
	SIM_PUSH_CONNECT,

	SIM_PUSH_SEND_IDLE,
	SIM_PUSH_SEND,

	SIM_PUSH_DISCONNECT_IDLE,
	SIM_PUSH_DISCONNECT,

	SIM_PUSH_FAILED = 30
} SIM800_PUSH_STATE;

typedef enum
{
	AUTO_CONNECT_STATE_IDLE						= 0,
    AUTO_CONNECT_STATE_INIT                     = 1,
    AUTO_CONNECT_STATE_WAIT_INIT_COMPLETE       = 2,

    AUTO_CONNECT_STATE_CONNECT_TO_DEST          = 3,
    AUTO_CONNECT_STATE_WAIT_CONNECTION          = 4,

    AUTO_CONNECT_STATE_SEND_MINE_DESTINATION    = 5,
    AUTO_CONNECT_STATE_WAIT_SEND_COMLETE        = 6,

    AUTO_CONNECT_STATE_DISCONNECT               = 7,
    AUTO_CONNECT_STATE_WAIT_DISCONNECT          = 8,

    AUTO_CONNECT_STATE_COMPLETED                = 9,
    AUTO_CONNECT_STATE_FAILED                   = 255
} AutoConnectState;

typedef struct SIM800_SMS_Instance_Struct
{
  	uint8_t* number;
	uint8_t* message;
} SIM800_SMS_Instance;

typedef struct SIM800_SMS_Struct
{
  	SIM800_SMS_Instance sms[SMS_COUNT];
	uint8_t count;
	uint8_t pointer;
} SIM800_SMS;

typedef struct StateSIM800Struct
{
	SIM800_GLOBAL_STATE			global;
	SIM800_POWER_UP_STATE		powerUp;
	SIM800_PROGRAM_INIT_STATE	programInit;
	SIM800_GPRS_INIT_STATE		gprsInit;
	SIM800_SERVER_INIT_STATE	serverInit;
	SIM800_SMS_INIT_STATE		smsInit;
	SIM800_SEND_DATA_STATE		sendData;
	SIM800_CLIENT_INIT_STATE	clientInit;
	SIM800_RECEIVE_DATA_STATE	receiveData;
	SIM800_PING_STATE			ping;
	SIM800_PUSH_STATE			push;
	AutoConnectState			autoConnect; 
} StateSIM800;

typedef struct CallbacksSIM800Struct
{
	SIM800_OnSim800CallbackStateType			onSim800ClientInit;
	SIM800_OnSim800DataReceiveCallbackType		onSim800DataReceive;
	SIM800_OnSim800CallbackEventType			onSim800NotResponse;
	SIM800_OnSim800CallbackStateType			onSim800GaspFinished;
} CallbacksSIM800;

typedef struct 
{
    uint8_t*			localState;
    uint8_t*			cmdPointer;
    const CMD_TypeDef*	cmdContainer;
} StateDescriptor;

typedef struct 
{
    uint8_t*			localState;
    uint8_t*			cmdPointer;
    const CMD_TypeDef*	cmdContainer;
	uint8_t 			failCode;
} LocalDiscriptor;

typedef struct SIM800_Struct
{
	// hardware resources
  	Pins_TypeDef*								powerPin;
	Pins_TypeDef*								resetPin;
	Uart_TypeDef*								serial;
	
	// sim800 uart buffers
	RingBuffer_t								rxBuffer;
	RingBuffer_t								txBuffer;
	
	// external uart buffers
	RingBuffer_t*								dataRxBuffer;
	RingBuffer_t*								dataTxBuffer;
	
	// timers
  	SWTimer_TypeDef								powerTimer;
	SWTimer_TypeDef								commandTimer;
	SWTimer_TypeDef								smsTimer;

	// current state variables
	uint8_t										lastCmd[60];
	LocalDiscriptor								discriptor;
	
	// rx data variables
	uint8_t										rxWait;
	uint8_t										tempBuffer[320];
	uint8_t										tempBufferPointer;
	
	// state pointers
	uint8_t										clientCmdPointer;
	uint8_t										smsCmdPointer;
	uint8_t										pushPointer;
	
	// state sim800
	StateSIM800 state;

	uint8_t										isRun;
	uint8_t										statusResponse;
	uint8_t										countAttempt;
	uint8_t										isOnOff;
	bool 										busyFlag;		// for synchronise async process

	// callbacks
	CallbacksSIM800								callbacks;
	
	// sms
	SIM800_SMS									smsList;
	SmsBuffer_t									smsBuffer;
	SMS_Instance								currentSms;

	//data gprs
	uint16_t									countDataBytes;
	RingBuffer_t								dataBytesBuffer;
	uint16_t									curCounterDataBytes;
	uint8_t										allowSendData;
	uint16_t									countBytesToSend;

	//responses
	uint8_t										responseValue[30];
	uint8_t										isNeedResponseValue;
	SIM800_RESPONSE_VALUE						responseValueState;

	//sim800 settings
	double										netQuality;
	double										balance;
	uint8_t										ipAddr[20];

	// push
	PushMessage									currentPushMessage;
	PushMsg_Buffer								pushBuffer;
	uint8_t										notifyServerConnect;

	// receive
	Packet_Buffer								packetBuffer;

	// modem settings
	AutoConnect      							autoConnect;
	GPRSSetup        							gprsSetup;
	PushSetupDest    							pushSetupDest;
} SIM800_TypeDef;

void SIM800_backupState(SIM800_TypeDef* sim800);
void SIM800_restoreState(SIM800_TypeDef* sim800);

typedef struct SIM800_TIMER_CLBCK_STRUCT
{
  	SIM800_TypeDef* sim800;
	uint8_t* cmd;
} SIM800_TIMER_CLBCK;

/**********************PUBLIC***********************/
void SIM800_Init(SIM800_TypeDef* sim800, RingBuffer_t* rxExternBuffer, RingBuffer_t* txExternBuffer);
void SIM800_Start(SIM800_TypeDef* sim800);
void SIM800_reboot(SIM800_TypeDef* sim800);
void SIM800_Routine(SIM800_TypeDef* sim800);
void SIM800_Stop(SIM800_TypeDef* sim800);

/***********************Getters************************/
SIM800_GLOBAL_STATE SIM800_GetState(SIM800_TypeDef* sim800);

/*************************************Set Callbacks**************************************/
void SIM800_SetOnGprsInitCallback(SIM800_TypeDef* sim800, void (*callback)(void*, SIM800_ERROR));
void SIM800_SetOnClientInitCallback(SIM800_TypeDef* sim800, void (*callback)(void*, SIM800_ERROR));
void SIM800_SetOnDataReceiveCallback(SIM800_TypeDef* sim800, void (*callback)(void*, uint16_t));
void SIM800_SetOnNotResponseCallback(SIM800_TypeDef* sim800, void(*callback)(void*));
void SIM800_SetOnGaspFinished(SIM800_TypeDef* sim800, void(*callback)(void*, SIM800_ERROR));

/*********************?????????***********************/
void sim800_off(SIM800_TypeDef* sim800);
void killConnections(SIM800_TypeDef* sim800);

/************Init SIM800 functions*********/
void programInitProcess(SIM800_TypeDef* sim800);
void SIM800_ProgramReset(SIM800_TypeDef* sim800);

/************Client functions************/
void SIM800_Client_Start(SIM800_TypeDef* sim800);
void clientInitProcess(SIM800_TypeDef* sim800);
void SIM800_ClientReset(SIM800_TypeDef* sim800);

/************Server functions************/
void SIM800_ServerReset(SIM800_TypeDef* sim800);

/************Gprs functions**************/
void gprsInitProcess(SIM800_TypeDef* sim800);				// state machine of gprs init
void SIM800_GprsReset(SIM800_TypeDef* sim800);				// reset state of Gprs

/*************Ping Sim800*****************/
void checkPing(SIM800_TypeDef* sim800);
void pingCallback(void* context);
void pingProcess(SIM800_TypeDef* sim800);

/*************PowerUp Sim800**************/
void powerUpprocess(SIM800_TypeDef* sim800);
void powerUpTimer(void* context);
void powerUpStep(SIM800_TypeDef* sim800, uint32_t delay);

/************GASP sim800******************/
void SIM800_StartGasp(SIM800_TypeDef* sim800);
void gaspProcess(SIM800_TypeDef* sim800);

/*************SMS Send process************/
void SIM800_SendSms(SIM800_TypeDef* sim800, uint8_t* tel, uint8_t* text);
void checkSms(SIM800_TypeDef* sim800);
void smsSendProcess(SIM800_TypeDef* sim800);
void smsInitTimer(void* context);

/*************Push Msgs process***********/
void checkPush(SIM800_TypeDef* sim800);
void pushMsgProcess(SIM800_TypeDef* sim800);
void pushSendDataCallback(void* context);

void SIM800_restoreConnectionState(SIM800_TypeDef* sim800);
void SIM800_backupConnectionState(SIM800_TypeDef* sim800);

/**************Commands Sim800****************/
void programInitTimer(void* context);
void sendCommand(SIM800_TypeDef* sim800, const uint8_t* cmd, uint32_t timeout, uint8_t needAnswer, int countData);
void resendCmd(SIM800_TypeDef* sim800);

/***************Uart Sim800 Buffers***********/
void checkRxBuffer(SIM800_TypeDef* sim800);

/***************Parse Strings*****************/
void checkReceivePackets(SIM800_TypeDef* sim800);
void parseGprsRcvData(SIM800_TypeDef* sim800, uint8_t d);
uint8_t parseErrorReceive(SIM800_TypeDef* sim800);
void checkResponseValue(SIM800_TypeDef* sim800, uint8_t* value);

/***************Reset & clear Commons**********/
void SIM800_resetFlags(SIM800_TypeDef* sim800);

/****************HW Callbacks*******************/
void OnSerialRead(void* context, uint8_t data);

/*****************Helpers***********************/
uint8_t isDigit(uint8_t ch);
uint8_t countOfDigit(uint8_t n);

#endif //_SIM800C_H