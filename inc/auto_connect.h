#ifndef __AUTO_CONNECT_H__
#define __AUTO_CONNECT_H__

#include "SIM800C.h"

/**
  * @brief Sets the reconnection permission flag
*/
void AUTOCONNECT_enableReconnect(void);

void AUTOCONNECT_handle(SIM800_TypeDef *sim800);

void AUTOCONNECT_init(void);

#endif // __AUTO_CONNECT_H__
