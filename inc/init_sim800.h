#ifndef __INIT_SIM800_H__
#define __INIT_SIM800_H__

#include "SIM800C.h"
#include "SimCmd.h"

#include <stdint.h>

void INIT_SIM800_handle(SIM800_TypeDef* sim800);
void INIT_SIM800_start(SIM800_TypeDef* sim800);
void INIT_SIM800_reset(SIM800_TypeDef* sim800);

#endif // __INIT_SIM800_H__
