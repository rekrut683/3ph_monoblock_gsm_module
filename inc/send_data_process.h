#ifndef __SEND_DATA_PROCESS_H__
#define __SEND_DATA_PROCESS_H__

#include "SIM800C.h"

void SENDDATA_handle(SIM800_TypeDef* sim800);
void SENDDATA_reset(void);
void SENDDATA_setExchangeAllowFlag(void);
void SENDDATA_setCompleteExchangeCallback(void(*callback)(void*));
void SENDDATA_resetCompleteExchangeCallback(void);

#endif // __SEND_DATA_PROCESS_H__
