#include "gprs.h" 

#include "debug.h"
#include "assistant.h"

#define COMMANDS_GPRS_SIZE  9

static const CMD_TypeDef commands[COMMANDS_GPRS_SIZE] = 
{
    {"AT+CGATT?\r\n", .resp = {"+CGATT: 1"}, .badResp = {""}, .error = E_GATT, .needAddRequest = 0, .timeoutResponse = 5000,    .needToRetry = 1}, // gprs status network
    {"AT+CIPSHUT\r\n", .resp = {"SHUT OK"}, .badResp = {""}, .error = E_RESET_CON, .needAddRequest = 0, .timeoutResponse = 35000,   .needToRetry = 1}, // reset all connections
    {"AT+IFC=2,2\r\n", .resp = {"OK"}, .badResp = {""}, .error = E_CONTEXT_GPRS, .needAddRequest = 0, .timeoutResponse = 10000,   .needToRetry = 1}, // hw flow control
    {"AT+CIPHEAD=1\r\n",        .resp = {"OK"},                  .badResp = {""},                .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 1},
    {"AT+CIPSRIP=1\r\n",        .resp = {"OK"},                  .badResp = {""},                .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 1},
    {"AT+CIPSTATUS\r\n",        .resp = {"STATE: IP INITIAL"},   .badResp = {""},                .error = E_IP_STATUS,       .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1}, // get status ip
    {"AT+CSTT=",                .resp = {"OK"},                  .badResp = {""},                .error = E_APN_STATUS,      .needAddRequest = 1,    .timeoutResponse = 3000,    .needToRetry = 1}, // set apn profile
    {"AT+CIICR\r\n",            .resp = {"OK"},                  .badResp = {""},                .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 30000,   .needToRetry = 1}, // activate context
    {"AT+CIFSR\r\n",            .resp = {"."},                   .badResp = {""},                .error = E_IP_ADDR,         .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 1}, // get ip address
};

static uint8_t currentCommand = 0;

/*************************Gprs functions********************************/
void GPRS_handle(SIM800_TypeDef* sim800)
{
    switch (sim800->state.gprsInit)
    {
        case SIM_GPRS_OFF:      // start gprs status network
        {
            DEBUG_sendString("\r\n------------------------------\r\n");
            DEBUG_sendString("* START GPRS INIT *");
            DEBUG_sendString("\r\n------------------------------\r\n");
    
            if (rBuffer_getCount(&sim800->rxBuffer) == 0)   // cgatt
            {
                sim800->state.gprsInit++;
                sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            }
            break;
        }
        case SIM_GPRS_STATUS_IDLE:        // waiting for
        case SIM_GPRS_RESET_CON_IDLE:
        case SIM_GPRS_HW_FLOW_IDLE:
        case SIM_GPRS_SET_HEAD_IDLE:
        case SIM_GPRS_REMOTEI_IDLE:
        case SIM_GPRS_IP_STATUS_IDLE:
        case SIM_GPRS_APN_IDLE:
        case SIM_GPRS_CONTEXT_IDLE:
        {
            break;
        }
        case SIM_GPRS_STATUS:
        {
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        }
        case SIM_GPRS_RESET_CON:    // start ip status
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        case SIM_GPRS_HW_FLOW:
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        case SIM_GPRS_SET_HEAD:
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        case SIM_GPRS_REMOTEI:
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
            // set APN data
        case SIM_GPRS_IP_STATUS:
            sim800->state.gprsInit++;
            char cmd[60] = {0};
            sprintf(cmd, "%s\"%s\",\"%s\",\"%s\"\r\n", commands[currentCommand].cmd, APN_NET_MTS, APN_UNAME, APN_PASS);
            sendCommand(sim800, (uint8_t*)cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
            // get status context
        case SIM_GPRS_APN:
            sim800->state.gprsInit++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
            // get self IP address
        case SIM_GPRS_CONTEXT:
            sim800->state.gprsInit++;
            sim800->responseValueState = SIM_IP;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 1, 0);
            break;
            // waiting for
        case SIM_GPRS_GET_IP_IDLE:
            break;
        case SIM_GPRS_GET_IP:
            sim800->state.gprsInit++;
            uint8_t cmdListen[60] = { 0 };

            // get base cmd
            strcat((char*)cmdListen, commands[currentCommand].cmd);
            ASSISTANT_sendNetAddress(cmdListen, sim800->autoConnect.destination[0]);
            sendCommand(sim800, &cmdListen[0], commands[currentCommand].timeoutResponse, 0, 0);
            break;
        case SIM_GPRS_READY_IDLE:
            sim800->state.gprsInit++;
            break;
        case SIM_GPRS_READY:
            DEBUG_sendString("\r\n------------------------------\r\n");
            DEBUG_sendString("* GPRS INIT SUCCESS *");
            DEBUG_sendString("\r\n------------------------------\r\n");

            sim800->state.global = SIM800_GPRS_READY;
            sim800->state.gprsInit++;
            break;
        case SIM_GPRS_FAILED:
            DEBUG_sendString("\r\n------------------------------\r\n");
            DEBUG_sendString("* GPRS INIT FAILED *");
            DEBUG_sendString("\r\n ERROR CODE:");
            uint8_t errorCode = E_RESET_CON;
            DEBUG_sendData(&errorCode, sizeof(errorCode));
            DEBUG_sendString("\r\n------------------------------\r\n");

            sim800_off(sim800);
            SIM800_reboot(sim800);
            break;
        default:
            break;
    }
}

void GPRS_start(SIM800_TypeDef* sim800)
{
    sim800->state.global = SIM800_GPRS_PROCESS;                 // set global state
    sim800->discriptor.localState   = &sim800->state.gprsInit;
    sim800->discriptor.failCode     = SIM_GPRS_FAILED;
    sim800->discriptor.cmdContainer = commands;
    sim800->discriptor.cmdPointer   = &currentCommand;
}

void GPRS_reset(SIM800_TypeDef* sim800)
{
    currentCommand = 0;
	sim800->state.gprsInit = SIM_GPRS_OFF;				// reset gprs state
	sim800->state.global = SIM800_GPRS_PROCESS;			// set global state Sim800
}
