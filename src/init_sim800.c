#include "init_sim800.h"

#include "debug.h"

#define COMMANDS_INIT_SIZE      5

const CMD_TypeDef commands[COMMANDS_INIT_SIZE] =
{
    {"AT\r\n", .resp = {"OK"}, .badResp = {""}, .error = E_SIM_LINK_UART, .needAddRequest = 0, .timeoutResponse = 2000,    .needToRetry = 1},
    {"AT+CREG?\r\n", .resp = {"+CREG: 0,1","+CREG: 0,5"}, .badResp = {""}, .error = E_SIM_SIMCARD,   .needAddRequest = 0, .timeoutResponse = 3000,    .needToRetry = 1},
    {"AT+CSQ\r\n", .resp = {"+CSQ:"}, .badResp = {""}, .error = E_SIM_QUALITY, .needAddRequest = 0, .timeoutResponse = 2000, .needToRetry = 1},
    {"AT+CUSD=1,", .resp = {"+CUSD:"}, .badResp = {""}, .error = E_NONE, .needAddRequest = 0, .timeoutResponse = 15000, .needToRetry = 1},
    {"AT+GSMBUSY=1\r\n", .resp = {"OK"}, .badResp = {""}, .error = E_NONE, .needAddRequest = 0, .timeoutResponse = 5000, .needToRetry = 1}
};

#define GSM_QUALITY_BAD_VALUE   11.0
static inline void completeInit(SIM800_TypeDef* sim800)
{
    uint8_t buffer[50] = {0};

    sprintf((char*)buffer, "\r\nSimcard balance: %.1f\r\n", sim800->balance);
    DEBUG_sendString(buffer);

    sprintf((char*)buffer, "GSM network quality signal: %.1f\r\n", sim800->netQuality);
    DEBUG_sendString(buffer);
    if (sim800->netQuality < GSM_QUALITY_BAD_VALUE)
    {
        DEBUG_sendString("WARNING: Very bad GSM signal\r\n");
    }
}

static uint8_t currentCommand = 0;

void INIT_SIM800_handle(SIM800_TypeDef* sim800)
{
    switch (sim800->state.programInit)
    {
    case SIM800_PROGRAM_OFF:
        DEBUG_sendString("\r\n------------------------------\r\n");
        DEBUG_sendString("* START INIT SIM800 *");
        DEBUG_sendString("\r\n------------------------------\r\n");

        sim800->state.programInit++;
        sendCommand(sim800, &commands[currentCommand].cmd[0], commands[currentCommand].timeoutResponse, 0, 0);
        break;

    case SIM800_AT_CHECK_IDLE:      // waiting for response on "AT" command
    case SIM800_CHECK_SIM_IDLE:     // waiting for simcard and operators checking
        break;
        // start simcard and operators checking [1]
    case SIM800_AT_CHECK:
        sim800->state.programInit++;
        sendCommand(sim800, &commands[currentCommand].cmd[0], commands[currentCommand].timeoutResponse, 0, 0);
        break;
        // start checking for signal quality [2]
    case SIM800_CHECK_SIM:
        sim800->state.programInit++;
        sim800->responseValueState = SIM_NET_QUALITY;
        sendCommand(sim800, &commands[currentCommand].cmd[0], commands[currentCommand].timeoutResponse, 1, 0);
        break;
        // waiting for signal quality checking
    case SIM800_CHECK_SIGNAL_IDLE:
        break;
    // start response for money balance on simcard [3]
    case SIM800_CHECK_SIGNAL:
        sim800->state.programInit++;
        sim800->responseValueState = SIM_BALANCE;
        char cmd[60] = { 0 };
        strcat(cmd, (char*)commands[currentCommand].cmd);
        strcat(cmd, "\"");
#if defined MTS
		strcat(cmd, USD_BALANCE_MTS);
#elif defined BEELINE
		strcat(cmd, USD_BALANCE_BEELINE);
#elif defined MEGAFON
		strcat(cmd, USD_BALANCE_MEGAFON);
#endif
		strcat(cmd, "\"");
		strcat(cmd, "\r\n");
		sendCommand(sim800, (const uint8_t*)cmd, commands[currentCommand].timeoutResponse, 1, 0);
		break;
	// waiting for money balance
	case SIM800_CHECK_BALANCE_IDLE:
		break;
	// start request for busy line [4]
	case SIM800_CHECK_BALANCE:
		sim800->state.programInit++;
		sendCommand(sim800, &commands[currentCommand].cmd[0], commands[currentCommand].timeoutResponse, 0, 0);
		break;
	// waiting for busy line
	case SIM800_BUSYGSM_IDLE:
		break;
	// start request for tlph number [5]
	case SIM800_BUSYGSM:
		sim800->state.programInit++;
#if defined MTS
		//strcat(cmdTlph, USD_NUMBERTLPH_BEELINE);
#elif defined BEELINE
		//strcat(cmdTlph, USD_NUMBERTLPH_BEELINE);
#elif define MEGAFON
		//strcat(cmdTlph, USD_NUMBERTLPH_BEELINE);
#endif
        break;
        // waiting for tlph number
    case SIM800_GETNUMBER_IDLE:
        sim800->state.programInit++;
        break;
        // finish
    case SIM800_GETNUMBER:
        sim800->state.programInit++;
        break;
        // finish
    case SIM800_PROGRAM_READY_IDLE:
        sim800->state.programInit++;
        break;
    case SIM800_PROGRAM_READY:
    {
        DEBUG_sendString("\r\n------------------------------\r\n");
        DEBUG_sendString("* INIT SIM800 SUCSESSUFUL *");
        DEBUG_sendString("\r\n------------------------------\r\n");

        sim800->state.global = SIM800_READY;
        sim800->state.programInit++;
        completeInit(sim800);
        break;
    }
    case SIM800_PROGRAM_FAILED:
    {
        DEBUG_sendString("\r\n------------------------------\r\n");
        DEBUG_sendString("* SIM800 INIT FAILED *");
        DEBUG_sendString("\r\n ERROR CODE:");
        uint8_t errorCode = commands[currentCommand].error;
        DEBUG_sendData(&errorCode, sizeof(errorCode));
        DEBUG_sendString("\r\n------------------------------\r\n");
        sim800_off(sim800);
        SIM800_reboot(sim800);
        break;
    }
    default:
        break;
    }
}

void INIT_SIM800_start(SIM800_TypeDef* sim800)
{
    sim800->state.global = SIM800_INIT_PROCESS;
    sim800->discriptor.localState   = &sim800->state.programInit;
    sim800->discriptor.failCode     = SIM800_PROGRAM_FAILED;
    sim800->discriptor.cmdContainer = commands;
    sim800->discriptor.cmdPointer   = &currentCommand;
}

void INIT_SIM800_reset(SIM800_TypeDef* sim800)
{
    currentCommand = 0;
    INIT_SIM800_start(sim800);
}
