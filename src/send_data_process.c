#include "send_data_process.h"

#define COMMANDS_SEND_DATA  (3)

#include "debug.h"

static const CMD_TypeDef commands[COMMANDS_SEND_DATA] = 
{
    {"AT+CIPSEND=", .resp = {">"},          .badResp = {"ERROR"},   .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 0},
    {"",            .resp = {0},            .badResp = {""},        .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 1},
    {"",            .resp = {"SEND OK"},    .badResp = {"ERROR"},   .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 0}
};
static uint8_t currentCommand = 0;
static bool allowSendData = false;
void(*completeExchangeCallback)(void*) = NULL;


static inline void endSendProcess(SIM800_TypeDef* sim800)
{
    SENDDATA_reset();
    sim800->state.sendData = SIM_SEND_IDLE;
    sim800->countBytesToSend = 0;

    SIM800_restoreState(sim800);

    rBuffer_clear(sim800->dataTxBuffer);

    // callback
    if (completeExchangeCallback)
    {
        completeExchangeCallback(sim800);
    }
}

void dataSendProcess(SIM800_TypeDef* sim800)
{
    switch (sim800->state.sendData)
    {
    case SIM_SEND_IDLE:
    case SIM_SEND_WAIT_INIT:
        break;
    case SIM_SEND_INIT:
        if (rBuffer_getCount(&sim800->rxBuffer) == 0)
        {
            sim800->state.sendData++;
            uint8_t cmdListen[40] = {0};
            sim800->countBytesToSend = rBuffer_getCount(sim800->dataTxBuffer);
            sprintf(cmdListen, "%s%d\r\n", commands[currentCommand].cmd, sim800->countBytesToSend);
            sendCommand(sim800, cmdListen, commands[currentCommand].timeoutResponse, 0, 0);
        }
        break;
    case SIM_SEND_PROCESS:
        sim800->state.sendData++;
        uint8_t dataToSend[BUFFER_SIZE] = {0};
        DEBUG_sendData((uint8_t*)&sim800->countBytesToSend, sizeof(sim800->countBytesToSend));
        for (uint16_t i = 0; i < sim800->countBytesToSend; i++)
        {
            dataToSend[i] = rBuffer_pop(sim800->dataTxBuffer);
        }

        dataToSend[sim800->countBytesToSend] = '\0';
        currentCommand++;
        sendCommand(sim800, dataToSend, commands[currentCommand].timeoutResponse, 0, sim800->countBytesToSend);
        break;
    case SIM_SEND_DATA_IDLE:
        sim800->state.sendData++;
        sendCommand(sim800, "\032", commands[currentCommand].timeoutResponse, 0, 0);
        break;
    case SIM_SEND_DATA:
        break;
    case SIM_SEND_DATA_OK:
    case SIM_SEND_DATA_ERROR:
        endSendProcess(sim800);
        break;
    }
}

void SENDDATA_reset(void)
{
    currentCommand = 0;
    allowSendData = false;
}

void SENDDATA_handle(SIM800_TypeDef* sim800)
{
    if (rBuffer_getCount(sim800->dataTxBuffer) > 0 &&
        (sim800->state.global == SIM800_SERVER_READY || sim800->state.global == SIM800_CLIENT_READY || sim800->state.global == SIM800_WAITING_MODE || sim800->state.global == SIM800_GPRS_PROCESS) &&
        sim800->state.sendData == SIM_SEND_IDLE && allowSendData)
    {
        sim800->state.sendData = SIM_SEND_INIT;
        SIM800_backupState(sim800);

        sim800->discriptor.failCode     = SIM_SEND_DATA_ERROR;
        sim800->discriptor.cmdContainer = commands;
        sim800->discriptor.cmdPointer   = &currentCommand;
        sim800->discriptor.localState   = &sim800->state.sendData;
    }

    dataSendProcess(sim800);
}

void SENDDATA_setExchangeAllowFlag(void)
{
    allowSendData = true;
}

void SENDDATA_setCompleteExchangeCallback(void(*callback)(void*))
{
    completeExchangeCallback = callback;
}
void SENDDATA_resetCompleteExchangeCallback(void)
{
    completeExchangeCallback = NULL;
}