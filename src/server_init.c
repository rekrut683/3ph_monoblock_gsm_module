#include "server_init.h"

#include "debug.h"

#define COMMANDS_SERVER_SIZE    2

static const CMD_TypeDef commands[COMMANDS_SERVER_SIZE] = 
{
    {"AT+CIPSHUT\r\n",      .resp = {"SHUT OK"},    .badResp = {""},        .error = E_RESET_CON,       .needAddRequest = 0,    .timeoutResponse = 35000,   .needToRetry = 1},
    {"AT+CIPSERVER=1,",     .resp = {"SERVER OK"},  .badResp = {"ERROR"},   .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 10000,   .needToRetry = 0}
};

static uint8_t currentCommand = 0;

static void sendDebugMessage(uint8_t *message)
{
    DEBUG_sendString("\r\n------------------------------\r\n");
    DEBUG_sendString(message);
    DEBUG_sendString("\r\n------------------------------\r\n"); 
}

void SERVER_INIT_handle(SIM800_TypeDef* sim800)
{
    switch (sim800->state.serverInit)
    {
        case SIM_SERVER_OFF:
        {
            if (rBuffer_getCount(&sim800->rxBuffer) == 0)
            {
                sendDebugMessage("* START SERVER INIT *");

                sim800->state.serverInit++;
                sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            }
            break;
        }
        case SIM_SERVER_RST_CON_IDLE:
        case SIM_SERVER_START_IDLE:
        {
            break;
        }
        case SIM_SERVER_RST_CON:
        {
            sim800->state.serverInit++;
            char cmdListen[40] = {0};
            sprintf(cmdListen, "%s%d\r\n", commands[currentCommand].cmd, LISTEN_PORT);

            sendCommand(sim800, (uint8_t*)cmdListen, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        }
        case SIM_SERVER_START:
        {
            sendDebugMessage("* SERVER INIT SUCCESS *");

            sim800->state.global = SIM800_SERVER_READY;
            sim800->state.serverInit++;
            break;
        }
        case SIM_SERVER_FAILED:
        {
            sim800_off(sim800);
            uint8_t message[45] = {0};
            sprintf((char*)message, "* SERVER INIT FAILED *\r\nERROR CODE:%d", commands[currentCommand].error);
            sendDebugMessage(message);

            SIM800_reboot(sim800);
            break;
        }
    }
}

void SERVER_INIT_start(SIM800_TypeDef* sim800)
{
    sim800->state.global = SIM800_SERVER_INIT_PROCESS;
    currentCommand = 0;
    sim800->state.serverInit = SIM_SERVER_OFF;

    sim800->discriptor.failCode     = SIM_SERVER_FAILED;
    sim800->discriptor.cmdContainer = commands;
    sim800->discriptor.cmdPointer   = &currentCommand;
    sim800->discriptor.localState   = &sim800->state.serverInit;
}

void SERVER_INIT_reset(SIM800_TypeDef* sim800)
{
    currentCommand = 0;
    sim800->state.serverInit = SIM_SERVER_OFF;
    sim800->state.global = SIM800_SERVER_INIT_PROCESS;
}
