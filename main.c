#include <stdint.h>
#include "CMSIS/inc/system_stm32f0xx.h"
#include "MyLib/gpio/gpioss.h"
#include "MyLib/pin/pins.h"
#include "MyLib/systimer/systimer.h"
#include "MyLib/swtimer/swtimer.h"
#include "MyLib/exti/exti.h"
#include "MyLib/dma/dma.h"
#include "MyLib/uart/uart.h"
#include "MyLib/timer/timers.h"
#include "MyLib/watchdog/watchdog.h"
#include "MyLib/adc/adc.h"
#include "SIM800C.h"
#include "RingBuffer.h"
#include <string.h>
#include <stdlib.h>
#include "PushMsgBuffer.h"

#include "auto_connect.h"
#include "debug.h"
#include "send_data_process.h"

#define GPRS
#define SERVER
//#define CLIENT

#define PIN_POWER_GSM				C2
#define PIN_RESET_GSM				C3
#define PIN_POWER_DETECT			C6

#define PIN_LED_OUT_STATUS 			C7
#define PIN_LED_OUT_NETLIGHT		C8

#define PIN_GSM_IN_STATUS			C0
#define PIN_GSM_IN_NETLIGHT			C1

#define PIN_IONISTOR_VOLTAGE		A7

#define UART_SERVICE				USART_3
#define UART_GSM					USART_2
#define UART_EXTERN					USART_1

#define INTERVAL_BTW_BYTES			2000			// 1500 uS on 9600 baudrate

#define INTERVAL_REQ_ION_VOLTAGE	2000
#define IONISTOR_REF_VALUE			1800			// ~1.4-1.5v

#define LEDS_LOCAL_INTERVAL			2000

#define DEBOUNCE_PWR_TIMEOUT		100
#define TURN_OFF_TIMER				30000

#define CONFIG_UPDATE_PERIOD		(5000)

typedef enum POWER_EVENT_ENUM
{
	NONE = 0,
	TURN_ON,
	TURN_OFF
} POWER_EVENT;

SIM800_TypeDef sim800 = 
	{
	 	.powerPin = PIN_POWER_GSM,
		.resetPin = PIN_RESET_GSM,
		.serial =	UART_GSM
	};

// power state
SWTimer_TypeDef debouncePwrDetectTimer;
SWTimer_TypeDef turnOffTimer;
uint8_t mainPowerState = 0;
POWER_EVENT pEvent = NONE;
uint8_t gaspFinished = 0;

// ionistor v
volatile uint16_t ionistorValue = 0;
SWTimer_TypeDef adcIonistorTimer;

// local leds control
SWTimer_TypeDef ledsTimer;
uint8_t allowInterruptsSim800 = 0;

// E-meter serial port
// data from GSM to E-meter
RingBuffer_t rxMeterBuffer;
uint8_t allowReceive;
uint16_t countDataReceive;
uint8_t* dataReceive = 0;
SWTimer_TypeDef configUpdateTimer;

// data from E-meter to GSM
RingBuffer_t	txMeterBuffer;

/***********************TEST**********************/
SWTimer_TypeDef smsTimer;
uint8_t smsFlag = 0;
uint8_t smsCount = 1;
uint8_t smsCounter = 0;

/******Push messages**************************/
uint8_t isPushMsg = 0;
/***************************************************/

/************LOCAL LEDS CONTROL************************/
void localLedsOn()
{
	// deny interrupts of sim800 pins
	allowInterruptsSim800 = 0;
	// off netlight pin
	Pin_Set(PIN_LED_OUT_NETLIGHT);
	// start timer
	SWTimer_Start(&ledsTimer, LEDS_LOCAL_INTERVAL);
}

void localLedsOff()
{
	SWTimer_Stop(&ledsTimer);
	allowInterruptsSim800 = 1;
}

void localLedsInterrupt(void* context)
{
	uint8_t value = Pin_Read(PIN_LED_OUT_STATUS);
	if (value == 0) Pin_Set(PIN_LED_OUT_STATUS);
	else Pin_Clear(PIN_LED_OUT_STATUS);
}

/*******************************************************/

/************************POWER**************************/
uint8_t getMainPowerState()
{
	return Pin_Read(PIN_POWER_DETECT);
}

POWER_EVENT checkPowerEvent()
{
	uint8_t p = getMainPowerState();
	if (mainPowerState != p)
	{
		// turn off main power
		if (mainPowerState == 1 && p == 0)
		{
			return TURN_OFF;
		}
		// turn on main power
		else
		{
			return TURN_ON;
		}
	}
	return NONE;
}

void turnOnPower()
{
    DEBUG_sendString("\r\nSystem Reset\r\n");
	SCB->AIRCR = 0x05fa0004;    // System RESET!!
}

void turnOffPower()
{
	SIM800_StartGasp(&sim800);
}

/**********************************************************/

/*******************CALLBACKS LEDS GSM************************/
void ledStatusGsmInterrupt(void* context)
{
	if (allowInterruptsSim800)
	{
		uint8_t value = Pin_Read(PIN_GSM_IN_STATUS);
		if (value == 0) Pin_Set(PIN_LED_OUT_STATUS);
		else Pin_Clear(PIN_LED_OUT_STATUS);
	}
}

void ledNetlightGsmInterrupt(void* context)
{
	if (allowInterruptsSim800)
	{
		uint8_t value = Pin_Read(PIN_GSM_IN_NETLIGHT);
		if (value == 0) Pin_Set(PIN_LED_OUT_NETLIGHT);
		else Pin_Clear(PIN_LED_OUT_NETLIGHT);
	}
}
/*************************************************************/

/******************POWER DETECT INTERRUPT*********************/
void turnOffCallback(void* context) {
	SWTimer_Stop(&turnOffTimer);
	turnOffPower();
}

void debouncePwrTimerCallback(void* context)
{
	SWTimer_Stop(&debouncePwrDetectTimer);
	pEvent = checkPowerEvent();
    DEBUG_sendString("\r\nInterrupt Handler\r\n");

    if (pEvent == TURN_OFF)
    {
        SWTimer_Start(&turnOffTimer, TURN_OFF_TIMER);
    }
    else if (pEvent == TURN_ON)
    {
        DEBUG_sendString("\r\nTurnOn Power\r\n");
        turnOnPower();
    }
    else
    {
        DEBUG_sendString("\r\nElse\r\n");
    }
	mainPowerState = getMainPowerState();
	Exti_On(EXTI6, RISING_FALLING, EXTI_Interrupt, 0);
}

void powerDetectInterrupt(void* context)
{
	Exti_Off(EXTI6);
	SWTimer_Start(&debouncePwrDetectTimer, DEBOUNCE_PWR_TIMEOUT);
}

/**************************************************************/

/****************OUTPUT DATA TO DEBUG SERIAL****************/
void checkDebugBuffer()
{
    if (DEBUG_getDataSize() > 0)
    {
        uint8_t byte = DEBUG_popByte();
        Usart_Send_Byte(UART_SERVICE, byte);
    }
}

/*****************OUTPUT DATA TO E-METER & DEBUG SERIAL********/
void checkRxExternBuffer()
{
    if (rBuffer_getCount(&rxMeterBuffer) > 0 && allowReceive)
    {
        uint16_t dSize = countDataReceive;
        dataReceive = (uint8_t*)malloc(countDataReceive);
        
        if (dataReceive != NULL)
        {
            memset(dataReceive, 0, countDataReceive);
            for (uint16_t i = 0; i < countDataReceive; i++)
            {
                dataReceive[i] = rBuffer_pop(&rxMeterBuffer);
            }
            Usart_Send_Data(UART_EXTERN, &dataReceive[0], countDataReceive);
            DEBUG_sendString("\r\nReceive: ");
            uint8_t cBytes[16];
            sprintf(cBytes, "%d %s\r\n", dSize, dataReceive);
            DEBUG_sendString(cBytes);

            free(dataReceive);
            allowReceive = 0;
            countDataReceive = 0;
        }
    }
}

// log error string to Service Uart
void putErrorString(uint8_t* text, SIM800_ERROR error)
{
    uint8_t message[50] = {0};
    strcat(message, text);
    uint8_t stringNum[4];
    sprintf(stringNum,"%d",error);
    strcat(message, stringNum);
    strcat(message, "\r\n");

    DEBUG_sendString(message);
}

// sim800 client connected
void sim800clientReadyCallback(void* context, SIM800_ERROR error)
{
    if(error == E_NONE)
    {
        DEBUG_sendString("\r\nSIM800 client CONNECTED OK\r\n");
    }
    else
    {
        putErrorString("\r\nSIM800 client connected ERROR, code: ", error);
#if defined CLIENT
        SIM800_Client_Start(&sim800);
        DEBUG_sendString("\r\nSIM800 start client ...\r\n");
#endif
    }
}

// sim800 gasp finished
void sim800gaspIsFinished(void* context, SIM800_ERROR error)
{
	gaspFinished = 1;
	if (error == E_NONE)
	{
		DEBUG_sendString("\r\nSIM800 GASP FINISHED OK\r\n");
	}
}

// data received from gprs net sim800
void sim800dataReceive(void* context, uint16_t countBytes)
{
	allowReceive = 1;
	countDataReceive = countBytes;
}

void sim800notAnswerCallback(void* context)
{

}
/*************************TEST SMS**********************************/
void smsCallback(void* context)
{
  	if (smsCounter < smsCount)
	{
		//79139164630
	  	//SIM800_SendSms(&sim800, "+79232436074", "Davlenie Prevysheno !!!");
		//SIM800_SendSms(&sim800, "+79139164630", "Nu i Ladno )");
		//SIM800_SendSms(&sim800, "+79139164630", "Nichego tut ne podelaesh :( ");
		smsCounter++;
	} else
	{
	  	smsCounter = 0;
	  	SWTimer_Stop(&smsTimer);
	}
}
/**************************************************************************/
/**************************************************************************/
#include "../intermoduleExchange/meter_exchange.h"
static void updateParametersCallback(void* context)
{
    if (METEREXCHANGE_needUpdate())
    {
        AutoConnect oldAutoConnectProp = sim800.autoConnect;
        METEREXCHANGE_getAutoConnect(&sim800.autoConnect);
        METEREXCHANGE_getGPRSSetup(&sim800.gprsSetup);
        METEREXCHANGE_getPushDestination(&sim800.pushSetupDest);

        if ((0 != memcmp(oldAutoConnectProp.destination[3], sim800.autoConnect.destination[3], sizeof(Destination))) ||
            (0 != memcmp(oldAutoConnectProp.destination[2], sim800.autoConnect.destination[2], sizeof(Destination))))
        {
            AUTOCONNECT_enableReconnect();
        }
    }
}

static void fillPushMessageCallback(const uint8_t *data, uint8_t size)
{
    pushBuffer_put(&sim800.pushBuffer, (uint8_t*)data, size);
}

// callback TIMER3. time between bytes from E-meter
void eMeterRcvBytesTimerOvf(void* context)
{
    Timer_Stop(TIMER3);
    Timer_SetCounterValue(TIMER3, 0);

    if (!METEREXCHANGE_handle(&txMeterBuffer))
    {
		SENDDATA_setExchangeAllowFlag();
    }
}

// callback E-meter. data came from E-meter
void eMeterUartCallbackReceive(void* context, uint8_t data)
{
    rBuffer_put(&txMeterBuffer, data);
    Timer_Stop(TIMER3);
    Timer_SetCounterValue(TIMER3, 0);
    Timer_Start(TIMER3);
}

// sleep function
void gotoSleep()
{

}

// adc callback. Measurement voltage of Ionistor completed
void ionistorVoltageCallback(void* context, uint16_t value)
{
	Adc_DisableIRQ(ADC_1);
	Adc_Stop(ADC_1);
	if (value < IONISTOR_REF_VALUE)
	{
		// do
		gotoSleep();
	}
	SWTimer_Start(&adcIonistorTimer, INTERVAL_REQ_ION_VOLTAGE);
}

// timer callback. time between voltage measurements of Ionistor
void ionistorTimerCallback(void* callback)
{
	ionistorValue = Adc_GetValue(ADC_1);
}

void serviceUartCallbackReceive(void* context, uint8_t size)
{

}

void Uarts_Init()
{
    // uart service init
    Usart_Enable(UART_SERVICE);
    Usart_Init(UART_SERVICE, 9600);
    Usart_SubscribeReceive(UART_SERVICE, 0, serviceUartCallbackReceive);
    
    // uart E_Meter init
    Usart_Enable(UART_EXTERN);
    Usart_Init(UART_EXTERN, 9600);
    Usart_SubscribeReceive(UART_EXTERN, 0, eMeterUartCallbackReceive);
}

void Buffers_Init()
{
    // ring buffers init
    DEBUG_init(512);
    rBuffer_init(&txMeterBuffer, 350);
    rBuffer_init(&rxMeterBuffer, 256);
}

void Timers_Init()
{
	// timer3 emeter receive bytes time shift overflow
	Timer_Enable(TIMER3);
	Timer_Init(TIMER3, 8, INTERVAL_BTW_BYTES, COUNTER_UP);			// timer 1 uS
	Timer_IRQEnable(TIMER3);
	Timer_IRQSubscribe(TIMER3, eMeterRcvBytesTimerOvf, 0);
}

void Pins_Init()
{
	// enable mcu leds pins
	Pin_Init(PIN_LED_OUT_STATUS, GPIO_Mode_OUT, HIGH_SPEED, PUSHPULL, NOPULL);
	Pin_Set(PIN_LED_OUT_STATUS);
	Pin_Init(PIN_LED_OUT_NETLIGHT, GPIO_Mode_OUT, HIGH_SPEED, PUSHPULL, NOPULL);
	Pin_Set(PIN_LED_OUT_NETLIGHT);
	// enable gsm status & net outputs (mcu inputs)
	Pin_Init(PIN_GSM_IN_STATUS, GPIO_Mode_IN, HIGH_SPEED, PUSHPULL, PUSH_UP);	
	Pin_Init(PIN_GSM_IN_NETLIGHT, GPIO_Mode_IN, HIGH_SPEED, PUSHPULL, PUSH_UP);
	// enable power detect input
	Pin_Init(PIN_POWER_DETECT, GPIO_Mode_IN, HIGH_SPEED, PUSHPULL, PUSH_UP);
	// ionistor voltage
	Pin_Init(PIN_IONISTOR_VOLTAGE, GPIO_Mode_AN, HIGH_SPEED, GPIO_OType_PP, GPIO_PuPd_NOPULL);
}

void Ionistor_Init()
{
	Adc_Init(ADC_1, ADC_ClockMode_AsynClk, 0);
	Adc_SetChannel(ADC_1, (*PIN_IONISTOR_VOLTAGE).AdcChannel, ADC_SampleTime_28_5Cycles);
	Adc_Start(ADC_1);
}

void ExInterrupt_Init()
{
	// led status GSM module
	Exti_Init(EXTI0, PIN_GSM_IN_STATUS, ledStatusGsmInterrupt, 0);
	Exti_On(EXTI0, RISING_FALLING, EXTI_Interrupt, 0);
	// led netlight GSM module
	Exti_Init(EXTI1, PIN_GSM_IN_NETLIGHT, ledNetlightGsmInterrupt, 0);
	Exti_On(EXTI1, RISING_FALLING, EXTI_Interrupt, 0);
	// power detect 12v
	Exti_Init(EXTI6, PIN_POWER_DETECT, powerDetectInterrupt, 0);
	Exti_On(EXTI6, RISING_FALLING, EXTI_Interrupt, 0); 
}

void Sim800_Init()
{
    // sim800 init
    SIM800_Init(&sim800, &rxMeterBuffer, &txMeterBuffer);
    SIM800_SetOnClientInitCallback(&sim800, sim800clientReadyCallback);
    SIM800_SetOnDataReceiveCallback(&sim800, sim800dataReceive);
    SIM800_SetOnNotResponseCallback(&sim800, sim800notAnswerCallback);
    SIM800_SetOnGaspFinished(&sim800, sim800gaspIsFinished);

    SIM800_Start(&sim800);
}

int main()
{
    // enable ports
    Port_Enable(PORTA);
    Port_Enable(PORTB);
    Port_Enable(PORTC);

    // sys Timer start 1 ms
    SysTimer_Start(SYSTICK_1MS);

    // watchdog
    Watchdog_Enable(IWDG_Prescaler_64, 0x0FFF);

	// init pins
	Pins_Init();
	
	// main power status
	mainPowerState = getMainPowerState();
    mainPowerState = 1;
	
	// adc ionistor init
	Ionistor_Init();
	
	// get ionistor voltage
	if (!mainPowerState)
	{
		ionistorValue = Adc_GetValue(ADC_1);
	}
	
	// init timer for local led control
	allowInterruptsSim800 = 1;
	SWTimer_Init(&ledsTimer);
	SWTimer_SubscribeCallback(&ledsTimer, 0, localLedsInterrupt);

	// init timer for debounce power detect
	SWTimer_Init(&debouncePwrDetectTimer);
	SWTimer_SubscribeCallback(&debouncePwrDetectTimer, 0, debouncePwrTimerCallback);
	SWTimer_Init(&turnOffTimer);
	SWTimer_SubscribeCallback(&turnOffTimer, 0, turnOffCallback);
	// external interrupt init
	ExInterrupt_Init();

	SWTimer_Init(&configUpdateTimer);
	SWTimer_SubscribeCallback(&configUpdateTimer, 0, updateParametersCallback);
	SWTimer_Start(&configUpdateTimer, CONFIG_UPDATE_PERIOD);

	uint8_t startWithoutMainPower = 0;

    METEREXCHANGE_subscribeFillPushMessageCallback(fillPushMessageCallback);
    AUTOCONNECT_init();

	if(!mainPowerState)
	{
		startWithoutMainPower = 1;
		localLedsOn();
	}
	else
	{
		Uarts_Init();
		Buffers_Init();
		Timers_Init();
		Sim800_Init();
	}
	
	while(1)
	{
		if (!startWithoutMainPower)
		{
			if (!mainPowerState && gaspFinished && sim800.state.global != SIM800_OFF)
			{
				gaspFinished = 0;
				SIM800_Stop(&sim800);
				localLedsOn();
			}
			SIM800_Routine(&sim800);
			checkDebugBuffer();
			checkRxExternBuffer();
		}
		Watchdog_Reset();
	}
}
