#include "PacketBuffer.h"
#include <string.h>

void packetBuffer_init(Packet_Buffer* rBuffer)
{
    rBuffer->size = MAX_PACKETS_BUFFER_SIZE;
}

void packetBuffer_put(Packet_Buffer* rBuffer, uint8_t* data, uint8_t count) {
	memcpy(rBuffer->packet[rBuffer->idxIn].packet, data, count);
	rBuffer->packet[rBuffer->idxIn].countBytes = count;
	rBuffer->packet[rBuffer->idxIn++].pointer = 0;
	if (rBuffer->idxIn >= rBuffer->size)
	{
		rBuffer->idxIn = 0;
	}
}

void packetBuffer_startPacket(Packet_Buffer* rBuffer, uint8_t position)
{
    rBuffer->packet[position].pointer = 0;
}

uint8_t packetBuffer_putByte(Packet_Buffer* rBuffer, uint8_t byte, uint8_t position) {
	rBuffer->packet[position].packet[rBuffer->packet[position].pointer++] = byte;
	return 1;
}

void packetBuffer_setPacket(Packet_Buffer* rBuffer, uint8_t dataCount, uint8_t position) {
	rBuffer->packet[rBuffer->idxIn].countBytes = dataCount;
	rBuffer->packet[rBuffer->idxIn++].pointer = 0;
	if (rBuffer->idxIn >= rBuffer->size)
	{
		rBuffer->idxIn = 0;
	}
}

PacketInstance_t* packetBuffer_pop(Packet_Buffer* rBuffer) {
	PacketInstance_t* retval = &rBuffer->packet[rBuffer->idxOut++];
	if (rBuffer->idxOut >= rBuffer->size)
	{
		rBuffer->idxOut = 0;
	}
	return retval;
}
uint16_t packetBuffer_getCount(Packet_Buffer* rBuffer) {
	uint16_t retval = 0;
	if (rBuffer->idxIn < rBuffer->idxOut)
	{
		retval = rBuffer->size + rBuffer->idxIn - rBuffer->idxOut;
	}
	else
	{
		retval = rBuffer->idxIn - rBuffer->idxOut;
	}
	return retval;
}

uint8_t packetBuffer_getPosition(Packet_Buffer* rBuffer) {
	return rBuffer->idxIn;
}